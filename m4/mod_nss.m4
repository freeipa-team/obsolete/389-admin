# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2007 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK

AC_CHECKING(for MOD_NSS)

# check for --with-modnss-lib
AC_MSG_CHECKING(for --with-modnss-lib)
AC_ARG_WITH(modnss-lib, [  --with-modnss-lib=PATH     MOD_NSS Apache module directory],
[
  if test -d "$withval"
  then
    AC_MSG_RESULT([using $withval])
    nssmoddir="$withval"
  else
    echo
    AC_MSG_ERROR([$withval not found])
  fi
],
AC_MSG_RESULT(no))

# check for --with-modnss-bin
AC_MSG_CHECKING(for --with-modnss-bin)
AC_ARG_WITH(modnss-bin, [  --with-modnss-bin=PATH     MOD_NSS binary directory],
[
  if test -d "$withval"
  then
    AC_MSG_RESULT([using $withval])
    modnssbindir="$withval"
  else
    echo
    AC_MSG_ERROR([$withval not found])
  fi
],
AC_MSG_RESULT(no))

# else, parse httpd.conf or httpconfdir/nss.conf
if test -z "$modnssbindir" ; then
    nsspcache=`grep NSSPassPhraseHelper $httpdconf|awk '{print $2}'`
    if test -z "$nsspcache" ; then
        nsspcache=`grep NSSPassPhraseHelper $httpd_root/conf.d/*.conf | awk '{print $2}'`
    fi
    if test -z "$nsspcache" ; then
        # same directory as Apache?
        modnssbindir=`dirname $HTTPD`
        nsspcache="$modnssbindir/nss_pcache"
    fi
    if test -z "$nsspcache" -o ! -f "$nsspcache" ; then
        AC_PATH_PROG([nsspcache], [nss_pcache], [],
	        		 [$PATH:/opt/hpws/apache/bin:/usr/local/apache/sbin:/usr/local/apache2/sbin:/usr/sbin])
    fi
    if test -n "$nsspcache" -a -f "$nsspcache" ; then
        modnssbindir=`dirname $nsspcache`
    else
        modnssbindir=
    fi
fi

if test -z "$nssmoddir" ; then
    nssmoddir="$moddir"
fi

if test ! -f "$modnssbindir/nss_pcache" ; then
    AC_MSG_ERROR([Could not find the mod_nss pass phrase helper $modnssbindir/nss_pcache])
else
    AC_MSG_RESULT([Using mod_nss pass phrase helper $modnssbindir/nss_pcache])
fi

if test ! -d "$nssmoddir" ; then
    AC_MSG_ERROR([Could not find the mod_nss module directory $nssmoddir])
else
    AC_MSG_RESULT([Using mod_nss module directory $nssmoddir])
fi
