# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2006 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK

AC_MSG_CHECKING(for --with-httpd)
AC_ARG_WITH(httpd,
            AS_HELP_STRING([--with-httpd=PATH],
                           [Full path of Apache binary.  Configure will usually find the right one, but if it does not, use this to specify the correct binary.]),
[
  if test -x "$withval"
  then
    AC_MSG_RESULT([using $withval])
    HTTPD="$withval"
  else
    echo
    AC_MSG_ERROR([$withval not found])
  fi
],
AC_MSG_RESULT(no))

if test -z "$HTTPD" ; then
    AC_PATH_PROG([HTTPD], [httpd.worker], [],
                 [$PATH:/opt/hpws/apache/bin:/usr/local/apache/sbin:/usr/local/apache2/sbin:/usr/sbin])
    if test -z "$HTTPD" -o ! -x "$HTTPD" ; then
        AC_PATH_PROG([HTTPD], [httpd], [],
                     [$PATH:/opt/hpws/apache/bin:/usr/local/apache/sbin:/usr/local/apache2/sbin:/usr/sbin])
    fi
    if test -z "$HTTPD" -o ! -x "$HTTPD" ; then
        AC_MSG_ERROR([Apache2 httpd server not found])
    fi
fi

httpd_root=`$HTTPD -V | grep HTTPD_ROOT | cut -f2 -d=`
httpd_root=`eval echo $httpd_root`
httpd_conf_rel=`$HTTPD -V | grep SERVER_CONFIG_FILE | cut -f2 -d=`
httpd_conf_rel=`eval echo $httpd_conf_rel`
# Server version: Apache/2.2.2
verstr='Server version: Apache/'
httpd_ver=`$HTTPD -v | grep "Server version:" | sed -e "s,$verstr,,"`
case $httpd_ver in
    2.4*) APACHE24=1 ;;
    2.2*) APACHE22=1 ;;
    *) APACHE22= ;;
esac

httpdconf=${httpd_root}/${httpd_conf_rel}

mimemagic=`awk '("MIMEMagicFile" == $1) {print $2}' $httpdconf`
if test ! -f "$mimemagic" ; then
# assume relative to root
    mimemagic=${httpd_root}/${mimemagic}
fi

# check for mime.types file
httpdconfdir=`dirname $httpdconf`
if test ! -f $httpdconfdir/mime.types ; then
    httpdconfdir="/etc"
    if test ! -f $httpdconfdir/mime.types ; then
        AC_MSG_ERROR([mime.types file not found])
    fi
fi

if test -z "$enable_threading" ; then
   enable_threading=yes # if not set on cmdline, set default
fi
AC_MSG_CHECKING(for --enable-threading)
AC_ARG_ENABLE(threading,
              AS_HELP_STRING([--enable-threading],
                             [Admin Server works best if Apache has threading support.  Use --disable-threading to force the use of an Apache without threading support.]))
AC_MSG_RESULT($enable_threading)

# check for worker model
AC_MSG_CHECKING(for $HTTPD with threading support)
httpd_model=`$HTTPD -V | grep "Server MPM:" | awk '{print $3}'`
if test "$httpd_model" = Worker ; then
    AC_MSG_RESULT([good - threading model is supported])
else
    httpd_model=`$HTTPD -V | grep "threaded:" | awk '{print $2}'`
    if test "$httpd_model" = yes ; then
        AC_MSG_RESULT([good - threading model is supported])
    else
        httpd_model=`$HTTPD -V | grep "APACHE_MPM_DIR" | grep "worker"`
        if test -n "httpd_model" ; then
            AC_MSG_RESULT([good - threading model is supported])
        elif test "$enable_threading" = yes ; then
            AC_MSG_ERROR([threading model not supported - use --disable-threading to force use of unthreaded model])
        else
            AC_MSG_RESULT([NOTICE - threading support explicitly disabled - Admin Server authorization cache will not work correctly])
        fi
    fi
fi

# check for --with-apxs
AC_MSG_CHECKING(for --with-apxs)
AC_ARG_WITH(apxs,
            AS_HELP_STRING([--with-apxs=PATH],
                           [Path to apxs]),
[
  if test -x "$withval"
  then
    AC_MSG_RESULT([using $withval])
    APXS=$withval
  else
    echo
    AC_MSG_ERROR([$withval not found or not executable])
  fi
],
AC_MSG_RESULT(no))

if test -z "$APXS" ; then
    # first look for APXS in same dir as HTTPD
    apachedir=`dirname $HTTPD`
    if test -x "$apachedir/apxs" ; then
        APXS="$apachedir/apxs"
    else
        AC_PATH_PROG([APXS], [apxs], [],
                     [$PATH:/opt/hpws/apache/bin:/usr/local/apache/sbin:/usr/local/apache2/sbin:/usr/sbin])
    fi
    if test -z "$APXS" ; then
        AC_MSG_ERROR([Apache2 apxs program not found])
    fi
fi

AC_CHECKING(for apr-config)
# check for --with-apr-config
AC_MSG_CHECKING(for --with-apr-config)
AC_ARG_WITH(apr-config,
            AS_HELP_STRING([--with-apr-config=PATH],
                           [Use apr-config to determine the APR directory.  Supply optional path to locate apr-config]),
[
  if test -x "$withval"
  then
    AC_MSG_RESULT([using $withval])
    APR_CONFIG=$withval
  fi
],
AC_MSG_RESULT(no))

if test -z "$APR_CONFIG" ; then
    # first look for APR_CONFIG in the bin dir if HTTPD is in the sbin dir
    apachedir=`dirname $HTTPD`
    parentdir=`dirname $apachedir`
    if test -x "$parentdir/bin/apr-1-config" ; then
        APR_CONFIG="$parentdir/bin/apr-1-config"
    elif test -x "$parentdir/bin/apr-config" ; then
        APR_CONFIG="$parentdir/bin/apr-config"
    else
        AC_PATH_PROGS(APR_CONFIG, apr-1-config apr-config, NO_APR_CONFIG,
                      [$PATH:/opt/hpws/apache/bin:/usr/local/apache/bin:/usr/bin])
    fi
    if test -z "$APR_CONFIG" ; then
        AC_MSG_ERROR([Apache2 apr-config program not found - please specify with --with-apr-config=/path/to/apr-config])
    fi
fi

if test -z "$moddir" ; then
    if test -n "$APXS" -a -x "$APXS" ; then
        moddir=`$APXS -q LIBEXECDIR`
    fi

    if test -z "$moddir" ; then
        moddir='$(libdir)/httpd/modules'
    fi
fi

dnl set the variables we need to build modules
apr_inc=`$APR_CONFIG --includes`
apache_inc=`$APXS -q INCLUDEDIR`
apache_conf=`$APXS -q SYSCONFDIR`
apache_prefix=`$APXS -q PREFIX`
apache_bin=`$APXS -q SBINDIR`
extra_cppflags=`$APXS -q EXTRA_CPPFLAGS` 

if ! test -f "$apache_inc/apr.h"; then
  if test -z "$apr_inc"; then
    AC_MSG_ERROR([apr.h is not in your Apache include dir as reported by apxs. Use --with-apxs to have apxs tell us where to find it.])
  fi
fi

dnl figure out the apr API version
apr_version=`$APR_CONFIG --version`
case $apr_version in
1.*) ap_ver_suf="-2.2" ;;
0.9*) ap_ver_suf= ;;
*) AC_MSG_ERROR(APR version $apr_version is not supported by this module) ;;
esac
