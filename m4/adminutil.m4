# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2006 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK

AC_CHECKING(for ADMINUTIL)

# check for --with-adminutil
AC_MSG_CHECKING(for --with-adminutil)
AC_ARG_WITH(adminutil, [  --with-adminutil=PATH   Adminutil directory],
[
  if test -d "$withval"/include -a -d "$withval"/lib
  then
    AC_MSG_RESULT([using $withval])
    ADMINUTILDIR=$withval
    adminutil_lib="-L$ADMINUTILDIR/lib"
    adminutil_libdir="$ADMINUTILDIR/lib"
    adminutil_incdir=$ADMINUTILDIR/include
    if ! test -e "$adminutil_incdir/libadminutil/admutil.h" ; then
      AC_MSG_ERROR([$withval include dir not found])
    fi
    adminutil_inc="-I$adminutil_incdir"
  else
    echo
    AC_MSG_ERROR([$withval not found])
  fi
],
AC_MSG_RESULT(no))

# if ADMINUTIL is not found yet, try pkg-config

# last resort
if test -z "$adminutil_inc" -o -z "$adminutil_lib"; then
  AC_MSG_CHECKING(for adminutil with pkg-config)
  AC_PATH_PROG(PKG_CONFIG, pkg-config)
  if test -n "$PKG_CONFIG"; then
    if $PKG_CONFIG --exists 389-adminutil; then
      adminutil_inc=`$PKG_CONFIG --cflags-only-I 389-adminutil`
      adminutil_lib=`$PKG_CONFIG --libs-only-L 389-adminutil`
      adminutil_libdir=`$PKG_CONFIG --libs-only-L 389-adminutil | sed -e s/-L// | sed -e s/\ .*$//`
    elif $PKG_CONFIG --exists adminutil; then
      adminutil_inc=`$PKG_CONFIG --cflags-only-I adminutil`
      adminutil_lib=`$PKG_CONFIG --libs-only-L adminutil`
      adminutil_libdir=`$PKG_CONFIG --libs-only-L adminutil | sed -e s/-L// | sed -e s/\ .*$//`
    else
      AC_MSG_ERROR([ADMINUTIL not found, specify with --with-adminutil.])
    fi
  fi
fi
