# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2007 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK

# check for --with-fortitude
default_fortitudedir=/opt/fortitude
AC_MSG_CHECKING(for --with-fortitude)
AC_ARG_WITH(fortitude,
            AS_HELP_STRING([--with-fortitude=PATH],
                           [Fortitude root directory (default: $default_fortitudedir)]),
[
  if test -d "$withval"
  then
    AC_MSG_RESULT([using $withval])
    fortitudedir="$withval"
  elif test "$withval" = "yes" -a -d $default_fortitudedir ; then
    AC_MSG_RESULT([using $default_fortitudedir])
    fortitudedir="$default_fortitudedir"
  else
    echo
    AC_MSG_ERROR([$withval not found])
  fi
],
AC_MSG_RESULT(no))

if test -n "$fortitudedir" ; then
    # see if there is a mod_nss in fortitude
    modnsslist=`ls $fortitudedir/modules.local/libmodnss.* 2> /dev/null`
    if test -n "$modnsslist" ; then
        nssmoddir="$fortitudedir/modules.local"
    fi
    # see if there is a nss_pcache
    if test -x "$fortitudedir/bin/nss_pcache" ; then
        modnssbindir="$fortitudedir/bin"
    fi
    # see if fortitude supplies apache
    if test -x "$fortitudedir/sbin/httpd.worker" ; then
        HTTPD="$fortitudedir/sbin/httpd.worker"
    fi
    # see if fortitude supplies apache modules
    if test -d "$fortitudedir/modules" ; then
        moddir="$fortitudedir/modules"
    fi
    # see if fortitude supplies apxs
    if test -x "$fortitudedir/sbin/apxs" ; then
        APXS="$fortitudedir/sbin/apxs"
    fi
    # see if fortitude supplies apr-config
    if test -x "$fortitudedir/bin/apr-config" ; then
        APR_CONFIG="$fortitudedir/bin/apr-config"
    fi
fi
