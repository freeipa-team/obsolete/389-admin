/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
//--------------------------------------------------------------------------//
//                                                                          //
//  Name: regparms.h                                                        //
//	Platforms: WIN32                                                        //
//  ......................................................................  //
//  This module contains registry key definations used throughout the       //
//  server.                                                                 //
//  ......................................................................  //
//
//--------------------------------------------------------------------------//
#define KEY_COMPANY             BRAND
#define KEY_APP_PATH            "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths"
#define KEY_RUN_ONCE            "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"
#define KEY_SNMP_SERVICE        "SNMP\\Parameters\\ExtensionAgents"
#define KEY_EVENTLOG_MESSAGES   "EventLogMessages"
#define VALUE_IMAGE_PATH        "ImagePath"
#define VALUE_CONFIG_PATH       "ConfigurationPath"
#define VALUE_ROOT_PATH         "RootPath"
#define VALUE_APP_PATH          "Pathname"
#define PROGRAM_GROUP_NAME      KEY_COMPANY " Server Family"
#define STR_PRODUCT_TYPE        "Server"
#define STR_EXE                 ".exe"

/* Admin IDs */
#define ADM_ID_PRODUCT         "admin"
#define ADM_NAME_SHORT         "Administration"
#define ADM_VERSION            ADM_PACKAGE_VERSION
#define ADM_NAME_VERSION       "Administration " ADM_VERSION
#define ADM_NAME_SERVER        "Administration Server"
#define ADM_NAME_FULL          KEY_COMPANY " " ADM_NAME_SERVER
#define ADM_NAME_FULL_VERSION  ADM_NAME_FULL " " ADM_VERSION
#define ADM_NAME_SERVICE       ADM_NAME_FULL_VERSION
#define ADM_EXE                "ns-admin.exe"
#define ADM_EXE_START          "admin.exe"
#define ADM_ID_SERVICE         "admin-serv"
#define ADM_KEY_ROOT           "Administration\\" ADM_VERSION
#define ADM_DIR_ROOT           "admin"
#define ADM_NAME_UNINSTALL     "Uninstall Administration Server " ADM_VERSION

/* original definitions */
// Upper-level registry parameters

#define SERVICE_NAME           ADM_ID_SERVICE
#define EVENTLOG_APPNAME	   ADM_NAME_VERSION
#define SERVICE_EXE            ADM_EXE
#define SERVICE_PREFIX         ADM_NAME_VERSION
#define SVR_ID_PRODUCT         ADM_ID_PRODUCT
#define SVR_NAME_SHORT         ADM_NAME_SHORT
#define SVR_VERSION            ADM_VERSION
#define SVR_NAME_VERSION       ADM_NAME_VERSION
#define SVR_NAME_SERVER        ADM_NAME_SERVER
#define SVR_NAME_FULL          ADM_NAME_FULL
#define SVR_NAME_FULL_VERSION  ADM_NAME_FULL_VERSION
#define SVR_NAME_SERVICE       ADM_NAME_SERVICE
#define SVR_EXE                ADM_EXE
#define SVR_EXE_START          ADM_EXE_START
#define SVR_ID_SERVICE         ADM_ID_SERVICE
#define SVR_KEY_ROOT           ADM_KEY_ROOT
#define SVR_DIR_ROOT           ADM_DIR_ROOT
#define SVR_NAME_UNINSTALL     ADM_NAME_UNINSTALL

#define VERSION_KEY "CurrentVersion"

// Configuration Parameters

#define SOFTWARE_KEY                    "Software"

// NT Perfmon DLL entries
#define KEY_PERFORMANCE     "Performance"
#define PERF_MICROSOFT_KEY	"SOFTWARE\\Microsoft\\Windows NT\\Perflib\\009"
#define PERF_COUNTER_KEY	"Counter"
#define PERF_HELP_KEY		"Help"
#define PERF_OPEN_FUNCTION	"OpenNSPerformanceData"
#define PERF_COLLECT_FUNCTION	"CollectNSPerformanceData"
#define PERF_CLOSE_FUNCTION	"CloseNSPerformanceData"
#define PERF_CTR_INI		"nsctrs.ini"
