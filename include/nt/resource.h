/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by netsite.rc
//
#define IDB_BITMAP1                     101
#define DLG_STARTUP_ERROR               101
#define IDI_NETSITE                     102
#define IDI_ICON1                       104
#define DLG_GETPASSWORD                 106
#define IDI_ICON2                       107
#define IDD_PASSWORD                    108
#define IDI_KEY                         109
#define IDD_PIN                         110
#define IDEDIT                          1000
#define IDC_ERRORLOG                    1001
#define ID_EXIT                         1001
#define IDC_ERRORMSG                    1002
#define IDC_STARTUP_ERROR               1003
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

