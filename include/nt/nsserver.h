/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/****************************************************************************
 *
 *   nsserver.h
 *
 ***************************************************************************/
#define NSDRV_LAUNCH_NAVIGATOR      DRV_USER
#define NSDRV_DRIVERS_REFRESH       DRV_USER+1
#define NSDRV_DRIVERS_INSTALLED     DRV_USER+2
#define NSDRV_DRIVER_STATUS         DRV_USER+3
#define NSDRV_DRIVER_START          DRV_USER+4
#define NSDRV_DRIVER_STOP	        DRV_USER+5
#define NSDRV_DRIVER_STARTUP        DRV_USER+6

typedef enum {
    SERVER_TYPE_ADMIN,
    SERVER_TYPE_FASTTRACK,
} SERVER_TYPE;

typedef enum {
    SERVER_STATUS_STOPPED,
    SERVER_STATUS_RUNNING,
} SERVER_STATUS;

typedef enum {
    SERVER_STARTUP_DISABLED,
    SERVER_STARTUP_MANUAL,
    SERVER_STARTUP_AUTOMATIC,
} SERVER_STARTUP;

typedef struct {
    int serverNumber;
    SERVER_TYPE type;
    SERVER_STATUS status;
    SERVER_STARTUP startup;
    char serverID[1];
} SERVER_INFO, * SERVER_INFO_PTR;
