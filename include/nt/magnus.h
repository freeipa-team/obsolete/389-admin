/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
//--------------------------------------------------------------------------//
//                                                                          //
//  Name: magnus.h                                                          //
//	Platforms: WIN32                                                        //
//  ......................................................................  //
//  This file contains the declarations of the server initialization        //
//  and control functions.                                                  //
//  ......................................................................  //
//  Revision History:                                                       //
//  01-12-95  Initial Version, Aruna Victor (aruna@netscape.com)            //
//  12-30-96  3.0 registry changes, cleanup, Andy Hakim (ahakim)            // 
//--------------------------------------------------------------------------//



//--------------------------------------------------------------------------//
// macros                                                                   //
//--------------------------------------------------------------------------//
#define CLOSEHANDLE(X) \
{ \
	if(X) \
	{ \
		CloseHandle(X); \
		X = 0; \
	} \
}


//--------------------------------------------------------------------------//
// function prototypes                                                      //
//--------------------------------------------------------------------------//

// functions in ntinit.c 
BOOL InitializeService();

// functions in ntmagnus.c 
NSAPI_PUBLIC int magnus_init();
NSAPI_PUBLIC void magnus_restart(int fs);
NSAPI_PUBLIC int threads_init();
NSAPI_PUBLIC VOID SuspendHttpdService();

