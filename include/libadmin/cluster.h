/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * cluster.h
 *
 * Revision History	By Whom		Note
 * 96/08/30		Adrian Chan	Initial Version
 */

#ifndef	CLUSTER_H
#define	CLUSTER_H

#define CL_RPT                  10      /* normal cluster table report style */
#define CL_RPT_NONE             20      /* no output from rexec */
#define CL_RPT_STDOUT           30      /* send rexec output to stdout */
 
#define CL_INFO                 0       /* normal status message */
#define CL_SUCC                 1       /* rexec succeeded */
#define CL_FAIL                 2       /* rexec failed */

#include <nspr.h>

typedef struct cluster_server {
   int  CLidx;			/* index in cluster_server_list struct */
   char *server_ident;		/* admin server's style server identifier */
   char *nsAdmin_URL;		/* full URL to admin server's */
   char *nsAdmin_protocol;	/* what protocol referred to in URL */
   char *nsAdmin_hostname;	/* what host referred to in URL */
   int  nsAdmin_port;		/* what port referred to in URL */
   char *request;		/* full request to this server */
   int  request_status;		/* status of remote execution */
} CL_server;

typedef struct cluster_server_list {
   int total_selected;		/* number of selected remote server */
   CL_server *server;		/* array of target servers */
} CL_server_list;

typedef struct cluster_request {
   char *product;		/* admin style to describe a product */
   unsigned char *auth_info;	/* authentication info about user */
   char *method;		/* method used to contact admin server */
   char *action;		/* relative or absolute path to command */
   char *expect;		/* success string to match from remote */
   char *form_data;		/* data to pass to remote for POST action */
   int  servers_executing;	/* how many servers left executing */
} CL_request;

NSPR_BEGIN_EXTERN_C

/* Parse HTTP header from sockd and match against remote_server_identifier */
NSAPI_PUBLIC int parse_http_header(PRFileDesc *sockd, bufstruct *nbuf, char *remote_server_identifier);

/* Monitor returned data for start/end of file tag: CF_BOUNDARY */
NSAPI_PUBLIC int parse_xfile_boundary(PRFileDesc *sockd, bufstruct *nbuf);

/* Monitor returned data for transfer file name: CF_NEWCONFIG */
NSAPI_PUBLIC int parse_xfilename(PRFileDesc *sockd, bufstruct *nbuf, char *filename);

/* message about empty cluster db */
NSAPI_PUBLIC int CL_emptyDatabase();

/* Get the remote admin server URL for specific server from cluster db */
NSAPI_PUBLIC int CL_getAdminURL(char *product, CL_server *server, int *errcode);

/* Get username/password from the environment */
NSAPI_PUBLIC int CL_getUserAuth(unsigned char **auth, int *errcode);

/* Get cluster request information submitted */
NSAPI_PUBLIC int CL_getRequest(CL_request *req, int *errcode);

/* Get all targeted cluster server information into server_list */
NSAPI_PUBLIC int CL_getTargetServers(char *product, CL_server_list *server_list, int *errcode);

/* Setup the cluster report layout engine */
NSAPI_PUBLIC int CL_reportLayout(CL_request *req, CL_server_list *server_list);

/* Update the status field */
NSAPI_PUBLIC int CL_reportStatus(int style, CL_server *server, CL_request *request, int type, char *mesg);

/* Added information to the details section */
NSAPI_PUBLIC int CL_reportDetails(int style, CL_server *server, CL_request *request, char *details);

/* Single remote exec */
NSAPI_PUBLIC int nsAdmin_rexec(CL_server *server, CL_request *request, int rpt_style);

/* Will go through server_list and call nsAdmin_rexec */
NSAPI_PUBLIC int CL_rexec(CL_request *req, CL_server_list *server_list, int *errcode);

NSPR_END_EXTERN_C

#endif	/* CLUSTER_H */
