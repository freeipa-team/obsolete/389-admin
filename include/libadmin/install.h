/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * install.h: Macros I use in the install programs
 * 
 * Rob McCool
 */


#ifndef NS_INSTALL_H
#define NS_INSTALL_H


#include <stdio.h>  /* FILE struct */

/* ------------------------------- Globals -------------------------------- */


/* ------------------------------ Structures ------------------------------ */


/* -------------------------------- Macros -------------------------------- */


/* ------------------------------ Prototypes ------------------------------ */

NSPR_BEGIN_EXTERN_C

int try_bind(char *addr, int port);

NSPR_END_EXTERN_C

#endif
