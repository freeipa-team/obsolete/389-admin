/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#ifndef SNMP_M_CONF
#define SNMP_M_CONF

typedef struct community_entry 
{
char * name;
char * operation;
} 
community_entry;

typedef struct community_list 
{
community_entry * community;
struct community_list  * next;
} 
community_list;

typedef struct manager_entry 
{
char * name;
char * trap_port;
char * community;
} 
manager_entry;

typedef struct manager_list 
{
manager_entry * manager;
struct manager_list  * next;
} 
manager_list;

/* 
input community name , operation, full path file name and flag 
   0  -- remove
   1  -- create/modify
*/
void modify_community(char *, char *, char *, int);

/* input full path of config file name */
community_list * get_community_list(char *);

/* input community_list and full path of config file name
   return 0 success, others fails*/
int write_community_list(community_list *, char *);

void free_community_all_list(community_list *);

/* assistant functions */
void free_community_entry(community_entry *);
void free_community_list(community_list *);


/* 
input manager name , trap port, community, full path file name and flag 
   0  -- remove
   1  -- create/modify
*/
void modify_manager(char *, char *, char *, char *, int);

/* input full path of config file name */
manager_list * get_manager_list(char *);

/* input community_list and full path of config file name
   return 0 success, others fails*/
int write_manager_list(manager_list *, char *);

void free_manager_all_list(manager_list *);

/* assistant functions */
void free_manager_entry(manager_entry *);
void free_manager_list(manager_list *);

#endif




