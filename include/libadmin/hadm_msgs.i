/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/* 
 * hadm_msgs.i: The text that can be returned from the httpd/newadmin/src 
 *              files
 *
 * Mike McCool
 */

/* Reserve 40 string ID's per program */

BGN_MSG(httpd_admsrc_msg)
STR(FORM_INVAL_QS, HADM_BASE+1, "You accessed this form with an invalid query string.")
STR(FORM_NOPOST, HADM_BASE+2, "This form does not work with POST.  Please access this form using only the pages provided.")
STR(ERR_NOSTAT, HADM_BASE+3, "Could not stat file.")
STR(ERR_FAIL_CREAT,	HADM_BASE+4, "Failed to create specified path object")
STR(ERR_FAIL_OPEN,	HADM_BASE+5, "Failed to open specified path object")
STR(ERR_FAIL_RENAME,	HADM_BASE+6, "rename() failed to update object")
STR(ERR_FAIL_UNIQUE,	HADM_BASE+7, "Failed to obtain unique name under")
STR(ERR_FAIL_AUTH,	HADM_BASE+8, "Failed authentication")
STR(ERR_FAIL_DNSLOOKUP,	HADM_BASE+9, "Failed DNS Lookup")
STR(ERR_FAIL_SSLSOCKET,	HADM_BASE+10, "Failed to obtain a new SSL socket")
STR(ERR_FAIL_SSLENABLE,	HADM_BASE+11, "Failed to enable SSL on socket")
STR(ERR_FAIL_SSLCONN,	HADM_BASE+12, "Failed to connect through SSL to remote host")
STR(ERR_FAIL_SSLREAD,	HADM_BASE+13, "Failed to read from SSL type socket")
STR(ERR_FAIL_SSLWRITE,	HADM_BASE+14, "Failed to write to SSL type socket")

STR(CLERR_BAD_HEADER,	HADM_BASE+40, "Invalid HTTP Header from remote")
STR(CLERR_BAD_URL,	HADM_BASE+41, "Specifed URL is not complete")
STR(CLERR_BAD_XFER,	HADM_BASE+42, "Remote server failure")
STR(CLERR_NOT_ADMIN,	HADM_BASE+43, "Remote is not a " CAPBRAND " Administration Server")
STR(CLERR_NOT_INIT,	HADM_BASE+44, "Must invoke from ns-admin")
STR(CLERR_NO_ACTION,	HADM_BASE+45, "Missing ACTION attribute")
STR(CLERR_NO_EXPECT,	HADM_BASE+46, "Missing EXPECT attribute string")
STR(CLERR_NO_HANDSHAKE,	HADM_BASE+47, "Can't contact remote admin server")
STR(CLERR_NO_MEM,	HADM_BASE+48, "Out of memory")
STR(CLERR_NO_METHOD,	HADM_BASE+49, "Missing METHOD attribute")
STR(CLERR_NO_MIXSEC,	HADM_BASE+50, "Can't mix secure/insecure environment")
STR(CLERR_NO_PASS,	HADM_BASE+51, "Missing password")
STR(CLERR_NO_PORT,	HADM_BASE+52, "Missing port number")
STR(CLERR_NO_PRODUCT,	HADM_BASE+53, "No cluster product is selected")
STR(CLERR_NO_PROTOCOL,	HADM_BASE+54, "Missing protocol attribute")
STR(CLERR_NO_RHOST,	HADM_BASE+55, "Missing remote host name")
STR(CLERR_NO_SERVER,	HADM_BASE+56, "No cluster server is specified")
STR(CLERR_NO_TARGET,	HADM_BASE+57, "No cluster server is selected")
STR(CLERR_NO_USER,	HADM_BASE+58, "Missing user information")
STR(CLXFER_ADM_RESPONSE, HADM_BASE+59, "Send: Request from %s")
STR(CLXFER_ADM_SENDFILE, HADM_BASE+60, "Sent: %s")
STR(CLXFER_ERR_OPENFILE, HADM_BASE+61, "%sUnable to open %s (%d).\n")
STR(CLXFER_NO_FILESEP,	HADM_BASE+62, "Missing start/stop file marker")
STR(CLXFER_NO_FILEMARK, HADM_BASE+63, "Missing or unmatched filename")

STR(CGI_DIR_WHICH, HADM_BASE+80, "Which entry?")
STR(CGI_DIR_LOG_DEL, HADM_BASE+81, "Remove: CGI directory %s")
STR(CGI_DIR_SUC_DEL, HADM_BASE+82, "Directory mapping removed.")
STR(CGI_DIR_MAP_NF, HADM_BASE+83, "Mapping not found")
STR(CGI_DIR_MAP_NF_L, HADM_BASE+84, "The mapping you tried to edit was not found.")
STR(CGI_DIR_RM_HTML, HADM_BASE+85, "<center><font size=+1>You can <a href=\"cgi-dir?path=%s&remove=1\">remove</a> this mapping. </font></center><hr size=4>\n")
STR(CGI_DIR_URL_PFX, HADM_BASE+86, "URL prefix")
STR(CGI_DIR_URL_PFX_L, HADM_BASE+87, "You should enter a prefix that the server can recognize and remap to the CGI directory.")
STR(CGI_DIR_DIR_MAP, HADM_BASE+88, "Directory to map to")
STR(CGI_DIR_DIR_MAP_L, HADM_BASE+89, "You should enter the name of the directory to use as a CGI directory.")

STR(CGI_DIR_ERR_BADDIR, HADM_BASE+90, "Bad directory to map to")
STR(CGI_DIR_ERR_BADDIR_L, HADM_BASE+91, "Please give an absolute path for the CGI directory.")
STR(CGI_DIR_LOG_CHNG, HADM_BASE+92, "Change: URL prefix %s, CGI dir %s")
STR(CGI_DIR_SUC_CHNG, HADM_BASE+93, "Mapping changed.")
STR(CGI_DIR_ERR_MAP_EX, HADM_BASE+94, "Mapping exists")
STR(CGI_DIR_ERR_MAP_EX_L, HADM_BASE+95, "That mapping already exists.  You can use <a href=viewmap>this form</a> to view your current mappings.")
STR(CGI_DIR_LOG_ADD, HADM_BASE+96, "Add: URL prefix %s, CGI dir %s")
STR(CGI_DIR_ERR_NOEX, HADM_BASE+97, "Warning: The directory %s does not exist.")
STR(CGI_DIR_SUC_ADD, HADM_BASE+98, "A CGI directory mapping has been added.")

STR(WINCGI_DIR_DEBUG, HADM_BASE+99, "Script Tracing")
STR(WINCGI_DIR_DEBUG_L, HADM_BASE+100, "You should specify if script tracing is on or off.")
STR(WINCGI_DIR_LOG_CHNG, HADM_BASE+101, "Change: Debug %s")
STR(WINCGI_DIR_LOG_ACT, HADM_BASE+102, "Activated WINCGI: %s")

STR(SHELLCGI_DIR_LOG_ACT, HADM_BASE+103, "Activated Shell CGI: %s")

STR(CGI_FT_ERR_ACT, HADM_BASE+120, "Activate / Deactivate CGI")
STR(CGI_FT_ERR_ACT_L, HADM_BASE+121, "You should specify whether CGI should be active or not.")
STR(CGI_FT_SUC_NOCHNG, HADM_BASE+122, "CGI preferences not changed.")
STR(CGI_FT_LOG_ACT, HADM_BASE+123, "Activated CGI: %s")
STR(CGI_FT_SUC_ACT, HADM_BASE+124, "CGI has been activated.")
STR(CGI_FT_LOG_DEACT, HADM_BASE+125, "Deactivated CGI: %s")
STR(CGI_FT_SUC_DEACT, HADM_BASE+126, "CGI has been deactivated.")

STR(CHARSET_LOG_CHARSET, HADM_BASE+160, "Charset: %s for %s")
STR(CHARSET_LOG_RMCHARSET, HADM_BASE+161, "Charset: Removed from %s")
STR(CHARSET_SUC_CHARSET, HADM_BASE+162, "Character set changed.")

STR(CHROOT_SUC_CHROOT, HADM_BASE+200, "Your server has been given a chroot directory.")

STR(COLLADDD_ERR_SELECT, HADM_BASE+240, "Select a collection")
STR(COLLADDD_ERR_SELECT_L, HADM_BASE+241, "Please choose a collection.")
STR(COLLADDD_ERR_ADD_DOCS, HADM_BASE+242, "Documents to add")
STR(COLLADDD_ERR_ADD_DOCS_L, HADM_BASE+243, "Please enter documents to add.")
STR(COLLADDD_ERR_OPEN_DOCROOT, HADM_BASE+244, "Could not open document root file.")
STR(COLLADDD_ERR_NOINSERT, HADM_BASE+245, "Could not insert files into collection.")
STR(COLLADDD_SUC_ADDED, HADM_BASE+246, "Documents have been added")

STR(COLLNEW_ERR_NAME, HADM_BASE+280, "Collection name")
STR(COLLNEW_ERR_NAME_L, HADM_BASE+281, "You should enter the name of the collection to create.")
STR(COLLNEW_ERR_EXISTS, HADM_BASE+282, "Collection exists")
STR(COLLNEW_ERR_EXISTS_L, HADM_BASE+283, "That collection already exists.  Either remove the old one or choose a new name.")
STR(COLLNEW_ERR_NODIR, HADM_BASE+284, "Directory to index")
STR(COLLNEW_ERR_NODIR_L, HADM_BASE+285, "You should enter the directory to index for this collection.")
STR(COLLNEW_ERR_BADDIR, HADM_BASE+286, "Bad directory to map to")
STR(COLLNEW_ERR_BADDIR_L, HADM_BASE+287, "Please give an <b>absolute path</b> for the directory to index.")
STR(COLLNEW_ERR_DESCS, HADM_BASE+288, "Descriptions")
STR(COLLNEW_ERR_DESCS_L, HADM_BASE+289, "Choose either to add descriptions, or not to.")
STR(COLLNEW_ERR_NODESC, HADM_BASE+290, "You chose to add a description, but did not enter one.")
STR(COLLNEW_ERR_DOCS, HADM_BASE+291, "Documents")
STR(COLLNEW_ERR_DOCS_L, HADM_BASE+292, "You should choose either to add documents or not to.")
STR(COLLNEW_ERR_NODOCS, HADM_BASE+293, "You chose to add a documents, but did not enter any.")
STR(COLLNEW_ERR_NOMAP, HADM_BASE+294, "No mapping to directory")
STR(COLLNEW_ERR_NOMAP_L, HADM_BASE+295, "Could not find any way for an HTTP user to access the directory to index.")

STR(COLLNEW_ERR_NOINSERT, HADM_BASE+296, "Collection creation failed (insert)")
STR(COLLNEW_ERR_NOINDEX, HADM_BASE+297, "Index files failed (insert)")
STR(COLLNEW_ERR_NOCREATE, HADM_BASE+298, "Collection creation failed (create)")
STR(COLLNEW_ERR_NOCREATE_L, HADM_BASE+299, "Collection creation failed (%d, create)")
STR(COLLNEW_ERR_WRITEDR, HADM_BASE+300, "Could not write docroot file")
STR(COLLNEW_ERR_WRITEUM, HADM_BASE+301, "Could not write URL map file")
STR(COLLNEW_SUCCESS, HADM_BASE+302, "A new collection has been created.")

STR(COLLOPT_ERR_SELECT, HADM_BASE+320, "Select a collection")
STR(COLLOPT_ERR_SELECT_L, HADM_BASE+321, "You should choose a collection.")
STR(COLLOPT_SUC_OPT, HADM_BASE+322, "Collection has been optimized.")

STR(COLLRM_ERR_SELECT, HADM_BASE+360, "Select a collection")
STR(COLLRM_ERR_SELECT_L, HADM_BASE+361, "You should choose a collection.")
STR(COLLRM_SUCCESS, HADM_BASE+362, "The collection has been removed.")

STR(COLLRMD_ERR_SELECT, HADM_BASE+400, "Select a collection")
STR(COLLRMD_ERR_SELECT_L, HADM_BASE+401, "You should choose a collection.")
STR(COLLRMD_ERR_REMOVE, HADM_BASE+402, "Remove")
STR(COLLRMD_ERR_REMOVE_L, HADM_BASE+403, "You should choose either to remove or not to.")
STR(COLLRMD_ERR_NODR, HADM_BASE+404, "Could not open document root file.")
STR(COLLRMD_ERR_NOREMOVE, HADM_BASE+405, "Failed to remove files from collection.")
STR(COLLRMD_SUCCESS, HADM_BASE+406, "Documents have been removed")

STR(COLLRP_ERR_SELECT, HADM_BASE+440, "Select a collection")
STR(COLLRP_ERR_SELECT_L, HADM_BASE+441, "You should choose a collection.")
STR(COLLRP_SUCCESS, HADM_BASE+442, "Collection repaired.")

STR(COLLUPD_ERR_SELECT, HADM_BASE+480, "Select a collection")
STR(COLLUPD_ERR_SELECT_L, HADM_BASE+481, "You should choose a collection.")
STR(COLLUPD_ERR_DOCS, HADM_BASE+482, "Add documents")
STR(COLLUPD_ERR_DOCS_L, HADM_BASE+483, "You should choose either to add documents or not to.")
STR(COLLUPD_ERR_NODOCS, HADM_BASE+484, "Could not open document root file.")
STR(COLLUPD_ERR_NOUPD, HADM_BASE+485, "Could not update files.")
STR(COLLUPD_SUCCESS, HADM_BASE+486, "Documents have been updated.")

STR(COLLVW_ERR_NODIR, HADM_BASE+520, "Could not open collection directory.")
STR(COLLVW_NOCOLLS, HADM_BASE+521, "<center><h3>No collections exist</h3></center>")
STR(COLLVW_VIEWTHIS, HADM_BASE+522, "View this collection")

STR(SWMHOME_WHICH, HADM_BASE+560, "Which entry?")
STR(SWMHOME_LOG_DEL, HADM_BASE+561, "Remove: URL host %s")
STR(SWMHOME_SUC_DEL, HADM_BASE+562, "URL host removed.")
STR(SWMHOME_HOME_NF, HADM_BASE+563, "Host not found")
STR(SWMHOME_HOME_NF_L, HADM_BASE+564, "The host you tried to edit was not found.")
STR(SWMHOME_LOG_CHNG, HADM_BASE+565, "Change: Host %s to page %s")
STR(SWMHOME_SUC_CHNG, HADM_BASE+566, "URL host changed.")
STR(SWMHOME_ERR_MAP_EX, HADM_BASE+567, "Mapping already exists")
STR(SWMHOME_ERR_MAP_EX_L, HADM_BASE+568, "That mapping already exists. Please use the edit button to edit it.")
STR(SWMHOME_LOG_ADD, HADM_BASE+569, "Add: URL host %s, page %s")
STR(SWMHOME_SUC_ADD, HADM_BASE+570, "Host mapping added.")
STR(SWMHOME_URL_HOST, HADM_BASE+571, "URL host")
STR(SWMHOME_URL_HOST_L, HADM_BASE+572, "You should enter a host will map to the given home page.")
STR(SWMHOME_URL_HOME, HADM_BASE+573, "Home page")
STR(SWMHOME_URL_HOME_L, HADM_BASE+574, "You should enter a file name from the document root, or a full path to a file to use as this virtual server's home page.")

STR(FILECMDS_ERR_ACT, HADM_BASE+575, "Activate / Deactivate file commands")
STR(FILECMDS_ERR_ACT_L, HADM_BASE+576, "Please specify whether or not you will allow file manipulation commands.")

STR(FILECMDS_SUC_NOCHNG, HADM_BASE+577, "File manipulation preferences not changed.")
STR(FILECMDS_LOG_ACT, HADM_BASE+578, "Activated file manipulation: %s")
STR(FILECMDS_SUC_ACT, HADM_BASE+579, "File manipulation has been activated.")
STR(FILECMDS_LOG_DEACT, HADM_BASE+580, "Deactivated file manipulation: %s")
STR(FILECMDS_SUC_DEACT, HADM_BASE+581, "File manipulation has been deactivated.")

END_MSG(httpd_admsrc_msg)
 
