/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#ifndef backup_h
#define backup_h

#include	<base/file.h>

typedef struct {
  char		*name;
  char		*path;
  char		*fullPath;
  int		version;
  long		timestamp;
  char		*extraInfo;	/* used by higher level backup software */
} ADM_bkFile;

/* A file and version to make a tag from */
typedef struct {
  int		file;		/* index of file */
  int		version;
} ADM_bkTagEl;

typedef struct {
  long		timestamp;
  int		numCohorts;
  ADM_bkTagEl	*cohorts;	/* what the file versions are */
  char		*log;
} ADM_bkTag;

typedef struct {
  int		modifiable;	/* 0 - r/o, 1 - can modify, > 1 - modified */
  long		quota;		/* how much disk space can backups take? */
  int		maxTags;	/* how many tags can backups take? */
  char		*root;		/* what directory files are relative to */
  char		*backupDir;
  SYS_FILE	f;
  int		numFiles;
  ADM_bkFile	*files;
  int		numTags;
  ADM_bkTag	*tags;
} ADM_bkTree;

typedef void (*ADM_bkAddResultFunc)( void *, ADM_bkTree *, int, int );

/***********************************************************************
** GLOBAL FUNCTIONS:
** DESCRIPTION:
** These functions implement a method of backing up a list of related
** files.
**
** EXAMPLE:
** This quicky looks at all the files mentioned in /tmp/backups/backups.conf
** and compares the modified times.  If any files are changed, it backs them
** up.
**
** ADM_bk_makeBackups( "/tmp/backups/backups.conf", "/tmp",
**		       "Meaningless Log Entry" );
**
** RESTRICTIONS:
** You must remember to use ADM_bk_initTree and ADM_bk_done, or the files
** may become inconsistent
***********************************************************************/

NSPR_BEGIN_EXTERN_C

NSAPI_PUBLIC int
ADM_bk_newConfFile( char *dir, char *fileName );

NSAPI_PUBLIC ADM_bkTree *
ADM_bk_initTree( char *root, char *confFile, int readOnly, int waitForLock );

NSAPI_PUBLIC void
ADM_bk_done( ADM_bkTree *tree );

NSAPI_PUBLIC int
ADM_bk_addFile( ADM_bkTree *tree, char *nickname, char *path,
		char *extraInfo, char *log );

NSAPI_PUBLIC int
ADM_bk_restoreFile( ADM_bkTree *tree, int file, int version );

NSAPI_PUBLIC int
ADM_bk_restoreToTime( ADM_bkTree *tree, long timestamp,
		      ADM_bkAddResultFunc, ADM_bkAddResultFunc,
		      void *parameter );

NSAPI_PUBLIC int
ADM_bk_doBackups( ADM_bkTree *tree, char *log );

NSAPI_PUBLIC int
ADM_bk_makeBackups( char *confFile, char *root, char *log );

NSAPI_PUBLIC int
ADM_bk_findFile( ADM_bkTree *tree, char *name );

NSAPI_PUBLIC void
ADM_bk_expire( ADM_bkTree * );

NSAPI_PUBLIC void
ADM_bk_setQuota( ADM_bkTree *, long );

NSAPI_PUBLIC void
ADM_bk_setMaxTags( ADM_bkTree *, int );

NSPR_END_EXTERN_C

#endif /* backup_h */
