/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * ds_base.h: base routines for batman admin
 *
 * Erh-Yuan Tsai
 */

#include "libadmin/libadmin.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
 
#ifdef XP_UNIX
#include <signal.h>
#include <ctype.h>
#include <sys/types.h>
#else
#include <windows.h>
#include <nt/regparms.h>
#include <winreg.h>
#include <base/ereport.h>
#endif

 
/* Batman has three configurations */
#define         CATALOG_SERVER  1
#define         RDS_SERVER      2
#define         MINI_SERVER     3
 
#ifdef XP_WIN32
#define COL_SIZE        60
#else
#define COL_SIZE        150
#endif

/* ----------------------------------------------- */
/* *** dynamic arrray *** */
typedef struct _dyn_array{
        int size;       /* current array size */
        int delta;      /* size to increase while overflow */
        int index;      /* next availabe slot */
        void **entry;
} dyn_array;

dyn_array *create_dyn_array(int size, int delta);
void destroy_dyn_array(dyn_array *ary, void(*free_elm)());
int dyn_add(dyn_array *ary, void *data);
void dyn_delete(dyn_array *ary, int inx);
void *dyn_data(dyn_array *ary, int index);
int dyn_size(dyn_array *ary);
/* ----------------------------------------------- */

/* *** utilities for batman admin *** */
int module_index(char *module);
char *module_name(int which);
char *module_dir(int which);

char *get_ns_home();
char *get_server_home();
char *get_batman_http_dir(int whichone, int module);
char *get_ds_dir(int whichone, int module);
char *get_ds_logs_dir(int whichone, int module);
char *get_ds_pid_file(int whichone, int module);
char *get_ds_conf_dir(int whichone, int module);
/* ----------------------------------------------- */

/* *** robot_cf.conf *** */
int put_robot_conf(int module, char *process, char *filter);
void get_robot_conf(int module, char *process, char *filter);
/* ----------------------------------------------- */

/* *** server.conf ie(rdm.conf) *** */
char *get_server_conf(int module);
void read_server_conf(int module);
void write_server_conf(int module);
char *get_server_var(int module, char *name);
int set_server_var(int module, char *name, char *val);

/* --------- OLD ROUTINES ---------------------- */

#ifndef BATMAN_ADMIN
void old_delete_mag_var(int offset);
void old_read_mag_conf(void);
void old_write_mag_conf(void);
pblock *old_get_mag_init(char *fn);
pblock *old_get_mag_init_ext(char *fn, char *key, char *data);
void old_set_mag_init(char *fn, char *key, char *val, int nargs, ...);
void old_delete_mag_init(char *fn);
void delete_mag_init_fun(char *fn, char *key, char *val);
char *get_mag_var(char *name);
void set_obj_var(char *action, char *fn, char *name, char *val);
void del_obj_var(char *action, char *fn);
void old_delete_mag_var(int offset);

/*  -- NT Registry -- */
/* These are needed only by NT server 1.12. */
char *get_http_dir();

#endif

/* ----------------------------------------------- */
/*  ---- CSID, csid.conf ----- */
/* ----------------------------------------------- */

typedef struct _csid {
        char *csid;		/* csid char string: x-catalog://<host>:<port>/<id_name> */
        char *verity_home;	/* verity home: 
				   .../plugins/ns-catalog/db/verity/common 
				*/
	char *verity_col;	/* verity collection:
				   .../plugins/ns-catalog/db/verity/collections/<id_name>
				*/
	char *alias;		/* filename for alias:
				  .../plugins/ns-catalog/config/alias.conf
				*/
	char *viewcache;	/* filename for catalog-database
				  .../plugins/ns-catalog/db/rd.db
				*/
	char *statistics;	/* filename for statistics log
				  .../logs/statistics.log
				*/
        char *dbdir;		/* database directory:
				  .../plugins/ns-catalog/db
				*/
	char *schema_d;		/* schema description file:
				  .../plugins/ns-catalog/db/schema.rdm
				*/
	char *server_d;		/* server description file:
				  .../plugins/ns-catalog/db/server.rdm
				*/
	char *taxonomy_d;	/* taxonomy description file:
				  .../plugins/ns-catalog/db/taxonomy.rdm
				*/
	char *import_c;		/* import configuration file:
				  .../plugins/ns-catalog/config/import.conf
				*/
} csid_type;

csid_type *create_csid();
void destroy_csid(void *a_csid);

#define MAX_CSID_TYPE	4
int type_index(char *buf);
char *type_name(int inx);

/* a structure for CSID */
typedef struct {
	char *host;
	char *port;
	char *server;
	int ssl;
} csid_stru;

csid_stru *csid2stru(char *csid);

dyn_array *get_csid_list(int module);

char *stru2csid(csid_stru *c_stru);
void free_csid_stru(csid_stru *c_stru);

char *get_csid_conf(int module);
dyn_array *read_csid_list(char *filename);
void write_csid_list(char *filename, dyn_array *ary);
void csid_delete(dyn_array *csid_ary, char *csid);
void csid_add(dyn_array *ary, csid_type *csid);
csid_type *get_csid(dyn_array *ary, char *name);
csid_type *dup_csid(csid_type *id);
dyn_array *set_default_csid();
char *get_csid_servid(int module);

/* ----------------------------------------------- */
void batman_success(char *description);
/* ----------------------------------------------- */


/* ----------------------------------------------- */
/*
   Routines stolean from netsite/lib/cs
*/
/* ----------------------------------------------- */
typedef unsigned int (*CSHashFn)(void *);
typedef int (*CSCompareFn)(void *, void *);
typedef void *(*CSCopyFn)(void *);
typedef void (*CSFreeFn)(void *);
typedef struct cs_list_node CSListNode;
typedef struct cs_list CSList;
typedef struct cs_list CSStack;
typedef struct cs_list CSQueue;
typedef struct cs_list CSPriQ;
typedef struct cs_hashtbl CSHashTable;
typedef struct cs_hashtblstats CSHashTableStats;
typedef struct _ncs_io_access_s CSIO;
 
#define CSIO_MEMORY             0x01
#define CSIO_FILE               0x10
#define CSIO_BOTH               ((CSIO_MEMORY)|(CSIO_FILE))
#define CSIO_IS_IN_FILE(p)      ((p)->location & CSIO_FILE)
#define CSIO_IS_IN_MEMORY(p)    ((p)->location & CSIO_MEMORY)
#define CSIO_BUFSZ              4096    /* 4k is good for disk */
 
struct _ncs_io_access_s {
        unsigned location;              /* Data is in memory, file, or both */
 
        char *data;                     /* Memory buffer */
        size_t datalen;                 /* len of valid data in data buffer */
        size_t current;                 /* current position in buffer */
        size_t allocated;               /* #bytes allocated in mem buffer */
        size_t max_alloc;               /* max memory buffer to allocate */
        size_t chunksize;               /* size of the chunks to allocate */
 
        char *filename;                 /* File which contains data */
        SYS_FILE fd;                    /* file pointer, for current */
        int is_temporary;               /* Is this a temporary file? */
        char *fdbuf;                    /* to speed up small appecs */
        int fdbufsz;                    /* amount of data in fdbuf */
};
 
struct cs_list_node {
        void *data;
        struct cs_list_node *next;
};
 
struct cs_list {
        CSListNode *head, *tail;
        unsigned int nnodes;
        CSFreeFn fr;
        CSCompareFn cmp;
        CSCopyFn cp;
};
 
 
struct cs_hashtbl {
        CSList **buckets;
        int hashsize;
        unsigned int nnodes;
        CSHashFn hf;
        CSCopyFn cp;
        CSCompareFn cmp;
        CSFreeFn fr;
};
 
struct cs_hashtblstats {
        int nobjs;
        int nbuckets;
        int longest_chain;
        int total_chain;
        double mean;
        double stddev;
};
 
typedef struct {
        pblock *pb;
        CSQueue *urls;
} ProcessNode;
 
extern CSQueue *processq;
void dsenum_load_process_cf(char *fn);
/* ----------------------------------------------- */
void do_stat_error(char *errstr, int print_errno);
int do_stat_log(char *success, char *statline);
/* ----------------------------------------------- */

/* Routines for setup cron jobs */
void setup_weekday(char *in_str, char *weekday);
void setup_schedule_time(char *time_str, char *days_str);
char *get_cgi_time();
char *get_cgi_day();
/* ----------------------------------------------- */

