/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/* 
 * la_msgs.i: The text that can be returned from the admin library
 *
 * Mike McCool
 */

/* Reserve 20 string ID's per script / libadmin object file. */

BGN_MSG(libadmin_msg)
STR(ADMLOG_ERR_NOOPEN, LA_BASE+1, "Could not open admin logfile.")
STR(ADMLOG_ERR_NOLOCK, LA_BASE+2, "Could not lock admin logfile.")
STR(ADMLOG_ERR_NOUNLOCK, LA_BASE+3, "Could not unlock admin logfile.")

STR(RES_ENTIRE_SERVER, LA_BASE+20, "the entire server")
STR(RES_OBJECT_NAMED, LA_BASE+21, "the object named %s")
STR(RES_DIRECTORY, LA_BASE+22, "the directory %s")
STR(RES_FILES, LA_BASE+23, "files matching %s")
STR(RES_FILE, LA_BASE+24, "the file %s")
#ifdef MCC_HTTPD /* template->style usability */
STR(RES_TMPL, LA_BASE+25, "the style %s")
#else
STR(RES_TMPL, LA_BASE+25, "the template %s")
#endif
STR(RES_WILD, LA_BASE+26, "files matching %s")

STR(ADMSERV_ERR_NOREAD_NSADM, LA_BASE+40, "Could not read configuration file <i>ns-admin.conf</i>.")
STR(ADMSERV_ERR_NOWRITE_NSADM, LA_BASE+41, "Could not write configuration file <i>ns-admin.conf</i>.")

STR(ADMSERV_LOG_SET_VAR, LA_BASE+42, "'%s' set to '%s'")
STR(ADMSERV_LOG_RM_VAR, LA_BASE+43, "'%s' removed")
STR(ADMSERV_LOG_ADD_VAR, LA_BASE+44, "'%s' added, value '%s'")

STR(ADMSERV_ERR_ADMDIR_NOREAD, LA_BASE+45, "Could not open admserv directory for server listing.")

END_MSG(libadmin_msg)
 
