/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * hinstall.h: Macros I use in the httpd install programs
 * 
 * Rob McCool
 */

#ifndef NS_HINSTALL_H
#define NS_HINSTALL_H

#include "install.h"

/* ------------------------------ Documents ------------------------------- */


#define SERVER_PAGE "server.html"
#define DEFAULT_SERVCONF "server.conf"
#define DOCS_PAGE "docs.html"
#define DEFAULT_DOCSCONF "docs.conf"
#define ADM_PAGE "admserv.html"

#ifdef MCC_PROXY
#define PROXY_PAGE "proxy.html"
#define DEFAULT_PROXYCONF "proxy.conf"
#endif /* MCC_PROXY */

#define REVIEW_PAGE "review.html"
#define FINISH_PAGE "finish.html"
#define UPGRADE_FORM "upgform.html"
#define UPGRADE_PAGE "upgrade.html"
#define QUIT_PAGE "quit.html"

/* Constants for the new server root */
#ifdef XP_UNIX
#ifdef MCC_PROXY
#define PRODUCT_DIR "proxy"
#define PRODUCT_BIN "ns-proxy"
#else	/* ! MCC_PROXY */
#ifdef MCC_HTTPD
#ifdef NS_UNSECURE
#define PRODUCT_DIR "httpd"
#define PRODUCT_BIN "ns-httpd"
#else
#define PRODUCT_DIR "https"
#define PRODUCT_BIN "ns-httpd"
#endif
#endif
#endif	/* ! MCC_PROXY */
#else /* XP_WIN32 */
#ifdef MCC_HTTPD
#ifdef NS_UNSECURE
#define PRODUCT_DIR "httpd"
#define PRODUCT_BIN "ns-httpd.exe"
#else
#define PRODUCT_DIR "https"
#define PRODUCT_BIN "ns-https.exe"
#endif
#endif
#endif /* XP_WIN32 */

/* ------------------------------ Structures ------------------------------ */


struct servconf {
    char *servname;
    char *servport;
    char *servaddr;
    char *servid;
    char *servroot;
    char *servuser;
#ifdef XP_WIN32
	char *password;
#endif /* XP_WIN32 */

    char *minprocs;
    char *maxprocs;

    char *dns;
    char *clf;

#ifdef MCC_PROXY
    char *ext;
#endif /* MCC_PROXY */

    char *slog;

    char **lines;
};


struct docsconf {
#ifdef MCC_PROXY

    int   proxy_http;			/* bool */
    int   proxy_ftp;			/* bool */
    int   proxy_gopher;			/* bool */
    int   proxy_https;			/* bool */
    int   tunnel_ssl_https;		/* bool */
    int   tunnel_ssl_snews;		/* bool */
    int   caching;			/* bool */
    char *cache_root;			/* directory */
    long  cache_size;			/* MB */
    long  cache_capacity;		/* MB */
    int   cache_ndirs;			/* 1..256 */
    int   cache_dim;			/* 0..8 */
    int   cache_http;			/* bool */
    int   cache_ftp;			/* bool */
    int   cache_gopher;			/* bool */
    int   cache_ims_always;		/* bool */
    long  cache_max_uncheck_http;	/* seconds */
    long  cache_max_uncheck_ftp;	/* seconds */
    long  cache_max_uncheck_gopher;	/* seconds */
    int   cache_use_lm;			/* bool */
    float cache_lm_factor;		/* float */

#else	/* ! MCC_PROXY */

    char *docroot;
    char *idxfiles;
    char *fancyidx;
    char *homepage;

#endif	/* !MCC_PROXY */

    char **lines;
};


/* ------------------------------ Prototypes ------------------------------ */


struct servconf *servconf_create(void);
void servconf_write(struct servconf *sc, char *fn);
struct servconf *servconf_scan(char *filename);

struct docsconf *docsconf_create(void);
void docsconf_write(struct docsconf *dc, char *fn);
struct docsconf *docsconf_scan(char *filename);

#endif
