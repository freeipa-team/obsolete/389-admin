/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#define LIBRARY_NAME "libadmin"

#include "i18n.h"

BEGIN_STR(libadmin)
	ResDef( DBT_LibraryID_, -1, dbtlibadminid )/* extracted from dbtlibadmin.h*/
	ResDef( DBT_help_, 1, "  Help  " )/*extracted from template.c*/
	ResDef( DBT_ok_, 2, "   OK   " )/*extracted from template.c*/
	ResDef( DBT_reset_, 3, " Reset " )/*extracted from template.c*/
	ResDef( DBT_done_, 4, "  Done  " )/*extracted from template.c*/
	ResDef( DBT_cancel_, 5, " Cancel " )/*extracted from template.c*/
END_STR(libadmin)
