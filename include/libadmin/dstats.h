/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/* -*- Mode: C; tab-width: 4 -*-
 *
 * dstats.h: Daemon statistics
 *
 *
 *
 *
 *
 * Ari Luotonen
 * Copyright (c) 1996 Netscape Communcations Corporation
 *
 */

#ifndef NS_DSTATS_H
#define NS_DSTATS_H

#include "base/daemon.h"


typedef struct {
    int idle, empty, active;
    int process, read, write, resolv;
} dstats;


typedef struct _DStatsHandle {

    /* private data */
    void * mapped;
    size_t mapped_size;

#ifdef XP_UNIX
    int    fd;
#else
    HANDLE mapHandle;
#endif

    StatHeader *hdr;
    StatSlot *stats;
    StatSlot totals;
    int nslots;
    int last_restart_cookie;

    /* public data */
    dstats snew;
    dstats sold;

} DStatsHandle;


NSPR_BEGIN_EXTERN_C

/*
 * Open the statistics file for the server running on the
 * specified port.
 *
 * Returns the handle to daemon statistics on success.
 * Returns NULL on failure, and sets 'err_title' and 'err_msg'
 * to point to an error title and message strings.
 *
 *
 */
NSAPI_PUBLIC DStatsHandle *dstats_open(int port, char *addr, char **err_title, char **err_msg);


/*
 * Grab and compile the current statistics from the statistics file.
 *
 *
 */
NSAPI_PUBLIC dstats *dstats_snapshot(DStatsHandle *handle);


/*
 * Un-mmap and close the statistics file, and free the statistics
 * context.
 *
 *
 */
NSAPI_PUBLIC void dstats_close(DStatsHandle *handle);



/*
 * dstats_poll_for_restart() returns true iff the server has
 * restarted since dstats_open() or the last call to
 * dstats_poll_for_restart().
 *
 * Return value is 1 if the server has soft restarted, in
 * which case it's ok to continue to use the same statistics.
 *
 * Return value is 2 if the server has hard restarted, and
 * the daemon stats file must be re-opened to re-attach to
 * active statistics.
 *
 */
NSAPI_PUBLIC int dstats_poll_for_restart(DStatsHandle *handle);

NSPR_END_EXTERN_C


#endif	/* ! NS_DSTATS_H */

