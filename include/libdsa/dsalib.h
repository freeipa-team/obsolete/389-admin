/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#ifndef __dsalib_h
#define __dsalib_h

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#ifdef HPUX
#include <limits.h> /* for PATH_MAX */
#endif
#include <lber.h>
#include <ldif.h>

/* error types */
#define DS_FILE_ERROR              0
#define DS_MEMORY_ERROR            1
#define DS_SYSTEM_ERROR            2
#define DS_INCORRECT_USAGE         3
#define DS_ELEM_MISSING            4
#define DS_REGISTRY_DATABASE_ERROR 5
#define DS_NETWORK_ERROR           6
#define DS_GENERAL_FAILURE         7
#define DS_WARNING                 8

/* The upper bound on error types */
#define DS_MAX_ERROR               9

/* The default error type (in case something goes wrong */
#define DS_DEFAULT_ERROR           3

#ifndef BIG_LINE
#define	BIG_LINE	1024
#endif
#ifndef PATH_MAX
#if defined( _WIN32 )
#define PATH_MAX _MAX_PATH
#else
#define PATH_MAX 256
#endif /* _WIN32 */
#endif /* PATH_MAX */
#ifndef HTML_ERRCOLOR
#define HTML_ERRCOLOR "#AA0000"
#endif
#ifndef CONTENT_NAME
#define CONTENT_NAME "content"
#endif

/* config file/path info */
#define DS_CONFIG_DIR      "DS_CONFIG_DIR"
#define DS_CONFIG_FILE     "dse.ldif"
#define DS_ORIGCONFIG_FILE "dse_original.ldif"

#define DS_CONFIG_LOCKDIR  "nsslapd-lockdir:"
#define DS_CONFIG_ERRLOG   "nsslapd-errorlog:"

#ifdef XP_UNIX

#define FILE_PATHSEP  '/'
#define FILE_PATHSEPP "/"
#define FILE_PARENT   "../"
#define WSACleanup()

#elif defined(XP_WIN32)

#define FILE_PATHSEP  '/'
#define FILE_PATHSEPP "\\\\"
#define FILE_PARENT   "..\\"

#endif /* XP_WIN32 */

#define PATH_SIZE 1024
#define ERR_SIZE 8192

/* 
   NT doesn't strictly need these, but the libadmin API which is emulated
   below uses them.
 */
#define NEWSCRIPT_MODE 0755
#define NEWFILE_MODE 0644
#define NEWDIR_MODE 0755

#if defined( XP_WIN32 )
#define DS_EXPORT_SYMBOL   __declspec( dllexport ) 
#else
#define DS_EXPORT_SYMBOL  
#endif

#if defined( XP_WIN32 )
#define ENQUOTE "\""
#else
#define ENQUOTE ""
#endif

#ifndef FILE_SEP
#ifdef XP_WIN32
  #define FILE_SEP '\\'
#else
  #define FILE_SEP '/'
#endif
#endif

/* only used if not WITH_SYSTEMD and not ENABLE_SERVICE */
#define START_SCRIPT "start-slapd"

#if defined( XP_WIN32 )
#define SLAPD_NAME "slapd"
#else
#define SLAPD_NAME "ns-slapd"
#endif

#define MOCHA_NAME "javascript"

/*
 * Return values from ds_get_updown_status()
 */
#define	DS_SERVER_UP		1
#define	DS_SERVER_DOWN		0
#define	DS_SERVER_UNKNOWN	-1
/*
 * Return values from ds_bring_up_server()
 */
#define	DS_SERVER_ALREADY_UP	-2
#define	DS_SERVER_ALREADY_DOWN	-3
#define	DS_SERVER_PORT_IN_USE		-4
#define	DS_SERVER_MAX_SEMAPHORES	-5
#define	DS_SERVER_CORRUPTED_DB		-6
#define	DS_SERVER_NO_RESOURCES		-7
#define	DS_SERVER_COULD_NOT_START	-8

/*
 * Other return values
 */
#define DS_UNKNOWN_ERROR            -1
#define DS_NO_INSTANCE_DIR         -10
#define DS_CANNOT_EXEC             -11
#define DS_CANNOT_OPEN_STAT_FILE   -12
#define DS_NULL_PARAMETER          -13
#define DS_SERVER_MUST_BE_DOWN     -14
#define DS_CANNOT_OPEN_BACKUP_FILE -15
#define DS_NOT_A_DIRECTORY         -16
#define DS_CANNOT_CREATE_DIRECTORY -17
#define DS_CANNOT_OPEN_LDIF_FILE   -18
#define DS_IS_A_DIRECTORY          -19
#define DS_CANNOT_CREATE_FILE      -20
#define DS_UNDEFINED_VARIABLE      -21
#define DS_NO_SUCH_FILE            -22
#define DS_CANNOT_DELETE_FILE      -23
#define DS_UNKNOWN_SNMP_COMMAND    -24
#define DS_NON_NUMERIC_VALUE       -25
#define DS_NO_LOGFILE_NAME         -26
#define DS_CANNOT_OPEN_LOG_FILE    -27
#define DS_HAS_TOBE_READONLY_MODE  -28
#define DS_INVALID_LDIF_FILE       -29

/*
 * Types of config files.
 */
#define	DS_REAL_CONFIG	1
#define	DS_TMP_CONFIG	2

/*
 * Maximum numeric value we will accept in admin interface
 * We may at some point need per-option bounds, but for now,
 * there's just one global maximum.
 */
#define	DS_MAX_NUMERIC_VALUE	4294967295	/* 2^32 - 1 */

/* Use our own macro for rpt_err, so we can put our own error code in
   NMC_STATUS */
#undef rpt_err
#define rpt_err(CODE, STR1, STR2, STR3) \
                     fprintf( stdout, "NMC_ErrInfo: %s\n", (STR1) ); \
                     fprintf( stdout, "NMC_STATUS: %d\n", CODE )

/*
 * Flags for ds_display_config()
 */
#define DS_DISP_HRB     1       /* horizontal line to begin with */
#define DS_DISP_HRE     2       /* horizontal line to end with */
#define DS_DISP_TB      4       /* table begin */
#define DS_DISP_TE      8       /* table end */
#define DS_DISP_EOL     16      /* End Of Line */
#define DS_DISP_NOMT    32      /* display only non empty */
#define DS_DISP_NOIN    64      /* display with no input field */
#define DS_DISP_HELP    128     /* display with a help button */
#define DS_DISP_PLAIN   256     /* No table, no nothin */
#define DS_SIMPLE       (DS_DISP_EOL | DS_DISP_NOIN | DS_DISP_HELP)
 
/*
 * dci_type for ds_cfg_info
 */
#define DS_ATTR_STRING  1
#define DS_ATTR_NUMBER  2
#define DS_ATTR_ONOFF   3
#define DS_ATTR_LIMIT   4       /* a number where -1 is displayed as blank */

struct ds_cfg_info {
        char    *dci_varname;
        char    *dci_display;
        int     dci_type;
        char	*dci_help;
};
 
extern struct ds_cfg_info ds_cfg_info[];

/*
 * varname for ds_showparam()
 * NOTE: these must be kept in synch with the ds_cfg_info array defined
 * in ../lib/dsalib_conf.c
 */
#define DS_LOGLEVEL     	0
#define DS_REFERRAL     	1
#define DS_AUDITFILE	   	2
#define DS_LOCALHOST    	3
#define DS_PORT			4
#define DS_SECURITY		5
#define DS_SECURE_PORT		6
#define DS_SSL3CIPHERS		7
#define DS_PASSWDHASH		8
#define DS_ACCESSLOG		9
#define DS_ERRORLOG		10
#define DS_ROOTDN		11
#define DS_ROOTPW		12
#define DS_SUFFIX       	13
#define DS_LOCALUSER		14
#define DS_BAKDIR		15
#define DS_TMPDIR		16
#define DS_INSTDIR		17
#define DS_CFG_MAX		18 /* MUST be one greater than the last option */

/* These control how long we wait for the server to start up or shutdown */
#define SERVER_START_TIMEOUT 600 /* seconds */
#define SERVER_STOP_TIMEOUT SERVER_START_TIMEOUT /* same as start timeout */

typedef int (*DS_RM_RF_ERR_FUNC)(const char *path, const char *op, void *arg);

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
extern DS_EXPORT_SYMBOL char *ds_get_instance_dir();
extern DS_EXPORT_SYMBOL char *ds_get_admserv_based_root();
extern DS_EXPORT_SYMBOL void ds_log_debug_message(char *msg);
extern DS_EXPORT_SYMBOL void ds_log_env(char **envp);
extern DS_EXPORT_SYMBOL int ds_get_updown_status();
extern DS_EXPORT_SYMBOL void ds_print_startstop(int stop);
extern DS_EXPORT_SYMBOL int ds_bring_up_server_install(int verbose,
	char *root, char *errorlog);
extern DS_EXPORT_SYMBOL int ds_bring_up_server(int verbose);
extern DS_EXPORT_SYMBOL char *ds_get_server_name();
extern DS_EXPORT_SYMBOL char *ds_get_short_name();
extern DS_EXPORT_SYMBOL void ds_send_error(char *errstr, int print_errno);
extern DS_EXPORT_SYMBOL void ds_send_status(char *str);
extern DS_EXPORT_SYMBOL char *ds_get_errors_name();
extern DS_EXPORT_SYMBOL char *ds_get_access_name();
extern DS_EXPORT_SYMBOL char *ds_get_audit_name();
extern DS_EXPORT_SYMBOL char *ds_get_logfile_name(int config_type);


extern DS_EXPORT_SYMBOL int ds_bring_down_server();
extern DS_EXPORT_SYMBOL void ds_print_server_status(int isrunning);
extern DS_EXPORT_SYMBOL int ds_get_file_size(char *fileName);
extern DS_EXPORT_SYMBOL void ds_display_tail(char *fileName, int timeOut, 
    int startSeek, char *doneMsg, char *lastLine);
extern DS_EXPORT_SYMBOL char **ds_get_bak_dirs();
extern DS_EXPORT_SYMBOL char **ds_get_config(int type);
extern DS_EXPORT_SYMBOL void ds_free_config(char **conf_list);
extern DS_EXPORT_SYMBOL char *ds_get_config_dir();
extern DS_EXPORT_SYMBOL void ds_set_config_dir(char *config_dir);
extern DS_EXPORT_SYMBOL char *ds_get_run_dir();
extern DS_EXPORT_SYMBOL void ds_set_run_dir(char *run_dir);
extern DS_EXPORT_SYMBOL char *ds_get_bak_dir();
extern DS_EXPORT_SYMBOL void ds_set_bak_dir(char *bak_dir);
extern DS_EXPORT_SYMBOL int ds_check_config(int type);
#if defined(USE_OPENLDAP)
extern DS_EXPORT_SYMBOL char **ds_get_conf_from_file(LDIFFP *conf);
#else
extern DS_EXPORT_SYMBOL char **ds_get_conf_from_file(FILE *conf);
#endif
extern DS_EXPORT_SYMBOL char *ds_get_var_name(int varnum);
extern DS_EXPORT_SYMBOL char *ds_get_value(char **ds_config, char *parm, int phase, int occurance);
extern DS_EXPORT_SYMBOL int ds_file_exists(char *filename);
extern DS_EXPORT_SYMBOL char *ds_get_config_value( int option );
extern DS_EXPORT_SYMBOL char **ds_get_file_list( char *dir );
extern DS_EXPORT_SYMBOL char *ds_get_tmp_dir();
extern DS_EXPORT_SYMBOL void ds_dostounixpath(char *szText);

/* Change the effective UID and GID of this process to
   those associated with the given localuser (if any). */
extern DS_EXPORT_SYMBOL char* ds_become_localuser_name (char* localuser);

/* Change the effective UID and GID of this process to
   those associated with ds_config's localuser (if any). */
extern DS_EXPORT_SYMBOL char* ds_become_localuser (char** ds_config);

/* Change the effective UID and GID of this process back to
   what they were before calling ds_become_localuser(). */
extern DS_EXPORT_SYMBOL char* ds_become_original();

/* Display an error to the user and exit from a CGI */
extern DS_EXPORT_SYMBOL void ds_report_error(int type, char *errmsg, char *details);

/* Display a warning to the user */
extern DS_EXPORT_SYMBOL void ds_report_warning(int type, char *errmsg, char *details);

/* show a message to be parsed by the non-HTML front end */
extern DS_EXPORT_SYMBOL void ds_show_message(const char *message);

extern DS_EXPORT_SYMBOL void alter_startup_line(char *startup_line);

extern DS_EXPORT_SYMBOL char *ds_system_errmsg(void);

/*
  remove a registry key and report an error message if unsuccessful
*/
extern DS_EXPORT_SYMBOL int ds_remove_reg_key(void *base, const char *format, ...);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __dsalib_h */
