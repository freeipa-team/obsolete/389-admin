/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#ifndef __dsalib_pw_h
#define __dsalib_pw_h

extern DS_EXPORT_SYMBOL void dsparm_help_button(char *var_name, char *dispname,
	char *helpinfo);
extern DS_EXPORT_SYMBOL LDAP* bind_as_root (char** cfg, char* rootdn, 
	char* rootpw);
extern DS_EXPORT_SYMBOL void get_pw_policy(char*** pValue, char** cfg);
extern DS_EXPORT_SYMBOL void ds_showpw( char** cfg);

#endif /* __dsalib_pw_h */
