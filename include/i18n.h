/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#ifndef I18N_H
#define I18N_H

/* Make NSAPI_PUBLIC available */
#include "base/systems.h"
#include "libadminutil/resource.h"

/*
 * Function prototypes for application and libraries
 */


#ifdef __cplusplus
extern "C" 
{
#endif

/******************************/
/* XP_GetStringFromDatabase() */
/******************************/

NSAPI_PUBLIC
extern char*
XP_GetStringFromDatabase(char* strLibraryName,
                         char* strLanguage,
                         int iToken);

#ifdef __cplusplus
}
#endif

/*******************************************************************************/

/*
 * Macros to simplify calls to XP_GetStringFromDatabase
 * (need one argument instead of three)
 */

#define XP_GetAdminStr(DBTTokenName)                   \
        XP_GetStringFromDatabase(LIBRARY_NAME,         \
                                 "en",   \
                                 DBTTokenName)

/*******************************************************************************/

#endif
