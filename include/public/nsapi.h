/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
#ifndef PUBLIC_NSAPI_H
#define PUBLIC_NSAPI_H

/*
 * File:        nsapi.h
 *
 * Description:
 *
 *      This file defines an interface for extending the server with
 *      in-process plug-ins.
 */

#include "base/systems.h"

#if defined(FILE_UNIX_MMAP) || defined(FILE_WIN32_MMAP) || defined(HAVE_MMAP)
#define FILE_MMAP
#if !defined(FILE_UNIX_MMAP) && (defined(HAVE_MMAP) && !defined(XP_WIN32))
#define FILE_UNIX_MMAP
#endif
#endif

/* --- Begin miscellaneous definitions --- */

/* Carriage return and line feed */
#define CR 13
#define LF 10

/* Return codes from file I/O routines */
#define IO_OKAY 1
#define IO_ERROR -1
#define IO_EOF 0
#define NETBUF_EOF	-1
#define NETBUF_ERROR	-2

/* The disk page size on this machine. */
#define FILE_BUFFERSIZE 4096

#ifdef XP_UNIX

#define FILE_PATHSEP '/'
#define FILE_PARENT "../"

#elif defined(XP_WIN32)

#define FILE_PATHSEP '/'
#define FILE_PARENT "..\\"

#endif /* XP_WIN32 */

/*
 * The REQ_ return codes. These codes are used to determine what the server
 * should do after a particular module completes its task.
 *
 * Func type functions return these as do many internal functions.
 */

/* The function performed its task, proceed with the request */
#define REQ_PROCEED 0
/* The entire request should be aborted: An error occurred */
#define REQ_ABORTED -1
/* The function performed no task, but proceed anyway. */
#define REQ_NOACTION -2
/* Tear down the session and exit */
#define REQ_EXIT -3
/* Restart the entire request-response process */
#define REQ_RESTART -4

/* --- End miscellaneous definitions --- */

/* --- Begin native platform includes --- */

#if defined(FILE_UNIX) || defined(FILE_UNIX_MMAP)
#include <sys/types.h>                  /* caddr_t */
#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#endif

#ifdef FILE_WIN32
#include <direct.h>
#endif /* FILE_WIN32 */

#ifdef NET_WINSOCK
#include <winsock.h>
struct iovec {
	char		*iov_base;
    unsigned	iov_len;
};
#else
#if !defined(SUNOS4) && !defined(HPUX)
#include <sys/select.h>
#endif
#include <sys/time.h>    /* struct timeval */
#include <sys/socket.h>
#include <netinet/in.h> /* sockaddr and in_addr */
#include <sys/uio.h>
#endif /* NET_WINSOCK */

#include <sys/stat.h>

#include <ctype.h>  /* isspace */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#ifdef XP_UNIX
#include <dirent.h>
#include <pwd.h>                /* struct passwd */
#endif /* XP_UNIX */
#include "libadminutil/psetc.h"

/* --- End native platform includes --- */

/* --- Begin type definitions --- */

#ifndef SYS_FILE_T
typedef void *SYS_FILE;
#define SYS_FILE_T void *
#endif /* !SYS_FILE_T */

#define SYS_ERROR_FD ((SYS_FILE)-1)

#ifndef SYS_NETFD_T
typedef void *SYS_NETFD;
#define SYS_NETFD_T void *
#endif /* !SYS_NETFD_T */

/* Error value for a SYS_NETFD */
#ifndef SYS_NET_ERRORFD
#define SYS_NET_ERRORFD ((SYS_NETFD)-1)
#endif /* !SYS_NET_ERRORFD */

/*
 * Type:        filebuffer, filebuf_t
 *
 * Description:
 *
 *      This structure is used to represent a buffered file.  On some
 *      systems the file may be memory-mapped.  A filebuffer is created
 *      by filebuf_open(), and destroyed by filebuf_close().
 *
 * Notes:
 *
 *      Direct access to the members of this structure, not using
 *      macros defined here, is discouraged.
 *
 *      The filebuf alias that used to be defined for this type was
 *      found to conflict with a C++ class of the same name, so it
 *      has been renamed to filebuf_t.
 *
 *      The inbuf field used to be (char *), but is now (unsigned char *)
 *      to simplify handling of 8-bit data.  The value returned by the
 *      filebuf_getc() macro is the (unsigned char) casted to (int), or
 *      an error code.  Unfortunately, IO_EOF cannot be distinguished
 *      from a zero byte, but a new function, filebuf_iseof(), is
 *      provided that will indicate definitively whether EOF has been
 *      reached.
 */

#ifdef FILE_MMAP

/* Version of filebuffer when memory-mapped files are supported */
typedef struct {
    SYS_FILE fd;
#ifdef FILE_UNIX_MMAP
    caddr_t fp;
#else /* FILE_WIN32_MMAP */
    HANDLE fdmap;
    char *fp;
#endif /* FILE_UNIX_MMAP */
    int len;

    unsigned char *inbuf;   /* for buffer_grab */
    int cursize;

    int pos;
    char *errmsg;
} filebuffer;

/* Return next character or IO_EOF */
#define filebuf_getc(b) ((b)->pos == (b)->len ? IO_EOF : (int)((b)->fp)[(b)->pos++])

#define filebuf_iseof(b) ((b)->pos == (b)->len)

#else

/* Version of filebuffer with no memory-mapped file support */
typedef struct {
    SYS_FILE fd;

    int pos, cursize, maxsize;
    unsigned char *inbuf;
    char *errmsg;
} filebuffer;

/* Return next character, IO_EOF, or IO_ERROR */
#define filebuf_getc(b) \
 ((b)->pos != (b)->cursize ? (int)((b)->inbuf[(b)->pos++]) : filebuf_next(b,1))

#endif /* FILE_MMAP */

/* C++ streamio defines a filebuf class. */
typedef filebuffer filebuf_t;

#ifdef XP_WIN32
/* Use a filebuffer to read data from a pipe */
#define pipebuf_getc(b) \
 ((b)->pos != (b)->cursize ? (int)((b)->inbuf[(b)->pos++]) : pipebuf_next(b,1))
#endif /* XP_WIN32 */

#ifdef XP_UNIX
typedef struct passwd *PASSWD;
typedef DIR* SYS_DIR;
typedef struct dirent SYS_DIRENT;
#endif /* XP_UNIX */

#ifdef XP_WIN32

typedef struct {
    char *d_name;
} dirent_s;

typedef struct {
    HANDLE dp;
    WIN32_FIND_DATA fdata;
    dirent_s de;
} dir_s;

typedef dir_s* SYS_DIR;
typedef dirent_s SYS_DIRENT;

#endif /* XP_WIN32 */

/* --- End type definitions --- */

#ifndef FILE_MMAP
#define filebuf_open_nostat(fd,sz,finfo) filebuf_open(fd,sz)
#endif

#ifdef XP_UNIX
#define dir_open opendir
#define dir_read readdir
#define dir_close closedir
#define dir_create(path) mkdir(path, 0755)
#define dir_remove rmdir
#define system_chdir chdir
#define file_unix2local(path,p2) strcpy(p2,path)
#endif /* XP_UNIX */

#ifdef XP_WIN32
#define dir_create _mkdir
#define dir_remove _rmdir
#define system_chdir SetCurrentDirectory
#endif /* XP_WIN32 */

/*
 * Thread-safe variants of localtime and gmtime
 */
#define system_localtime(curtime, ret) util_localtime(curtime, ret)
#define system_gmtime(curtime, ret) util_gmtime(curtime, ret)

/* --- OBSOLETE ----------------------------------------------------------
 * The following macros/functions are obsolete and are only maintained for
 * compatibility.  Do not use them. 11-19-96
 * -----------------------------------------------------------------------
 */

#define SYS_STDERR STDERR_FILENO

#ifdef XP_WIN32

typedef HANDLE pid_t;

#define ERROR_PIPE \
	(ERROR_BROKEN_PIPE | ERROR_BAD_PIPE |\
	 ERROR_PIPE_BUSY | ERROR_PIPE_LISTENING | ERROR_PIPE_NOT_CONNECTED)
#define CONVERT_TO_PRINTABLE_FORMAT(Filename) 	\
{									   		\
	register char *s;					   	\
	if (Filename)				   			\
	for (s = Filename; *s; s++) 	   		\
		if ( *s	== '\\')				   	\
			*s = '/';				   		\
}
#define CONVERT_TO_NATIVE_FS(Filename) 	   \
{									   	   \
	register char *s;				   	   \
	if (Filename)						   \
		for (s = Filename; *s; s++) 	   \
			if ( *s	== '/')				   \
				*s = '\\';				   \
}									 

#else /* !XP_WIN32 */

#endif /* XP_WIN32 */

#endif /* !PUBLIC_NSAPI_H */
