#!/bin/sh

testdir="$1"
hostname=vmhost.testdomain.com
domain=testdomain.com
sroot=/home/$USER/dsol
port=1200
secport=1201
rootdn="cn=directory manager"
escapedrootdn='cn\\\\3Ddirectory manager'
rootpw=password
adminpw=boguspassword
adminpw=admin
#needinstance=1
#needdata=1
usessl=1
secdir=/home/$USER/save
#PATH=/usr/lib64/mozldap:$PATH
#export PATH
instance=ds

if [ "$needinstance" ] ; then
$sroot/sbin/setup-ds.pl -s -f - <<EOF
[General]
FullMachineName=   $hostname
SuiteSpotUserID=   $USER
[slapd]
ServerPort=   $port
ServerIdentifier=   $instance
Suffix=   o=NetscapeRoot
RootDN=   $rootdn
RootDNPwd=  $rootpw
EOF

sslconf=/tmp/sslconf.$$.ldif
cat > $sslconf <<EOF
dn: cn=encryption,cn=config
changetype: modify
replace: nsSSL3
nsSSL3: on
-
replace: nsSSLClientAuth
nsSSLClientAuth: allowed
-
add: nsSSL3Ciphers
nsSSL3Ciphers: -rsa_null_md5,+rsa_rc4_128_md5,+rsa_rc4_40_md5,+rsa_rc2_40_md5,
 +rsa_des_sha,+rsa_fips_des_sha,+rsa_3des_sha,+rsa_fips_3des_sha,+fortezza,
 +fortezza_rc4_128_sha,+fortezza_null,+tls_rsa_export1024_with_rc4_56_sha,
 +tls_rsa_export1024_with_des_cbc_sha

dn: cn=config
changetype: modify
add: nsslapd-security
nsslapd-security: on
-
replace: nsslapd-ssl-check-hostname
nsslapd-ssl-check-hostname: off
-
replace: nsslapd-secureport
nsslapd-secureport: $secport

dn: cn=RSA,cn=encryption,cn=config
changetype: add
objectclass: top
objectclass: nsEncryptionModule
cn: RSA
nsSSLPersonalitySSL: Server-Cert
nsSSLToken: internal (software)
nsSSLActivation: on

EOF

ldapmodify -x -h $hostname -p $port -D "$rootdn" -w "$rootpw" -c -f $sslconf
rm -f $sslconf

$sroot/lib/dirsrv/slapd-$instance/stop-slapd
cp $secdir/*.db $sroot/etc/dirsrv/slapd-$instance
cp $secdir/pin.txt $sroot/etc/dirsrv/slapd-$instance
$sroot/lib/dirsrv/slapd-$instance/start-slapd

fi

if [ "$needdata" ] ; then
    for file in $testdir/*.ldif.tmpl $testdir/*.mod.tmpl ; do
        echo processing file $file
        tmpfile=/tmp/mod.$$
        sed \
            -e "s/%as_uid%/admin/g" \
            -e "s/%as_passwd%/admin/g" \
            -e "s/%domain%/$domain/g" \
            -e "s/%console_version%/0.0/g" \
            -e "s/%as_baseversion%/0.0/g" \
            -e "s/%ds_console_jar%/389-ds.jar/g" \
            -e "s/%fqdn%/$hostname/g" \
            -e "s/%ds_port%/$port/g" \
            -e "s/%ds_secure_port%/$secport/g" \
            -e "s/%ds_suffix%/$suffix/g" \
            -e "s/%ds_user%/$USER/g" \
            -e "s/%brand%/389/g" \
            -e "s/%dsid%/$instance/g" \
            -e "s,%uname_a%,`uname -a`,g" \
            -e "s/%uname_m%/`uname -m`/g" \
            -e "s/%ds_version%/0.0/g" \
            -e "s/%ds_buildnum%/0.0/g" \
            -e "s/%asid%/$instance/g" \
            -e "s/%vendor%/389/g" \
            -e "s/%timestamp%/`date +%Y%m%d%H%M%S`/g" \
            -e "s/%rootdn%/$rootdn/g" \
            -e "s/%escapedrootdn%/$escapedrootdn/g" \
            -e "s/%ds_sie%/cn=slapd-$instance,cn=389 Directory Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot/g" \
            -e "s/%as_sie%/cn=admin-serv-$instance,cn=389 Administration Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot/g" \
            -e "s/%as_version%/0.0/g" \
            -e "s/%as_buildnum%/0.0/g" \
            -e "s/%as_console_jar%/389-admin.jar/g" \
            -e "s/%as_port%/9830/g" \
            -e "s/%as_user%/$USER/g" \
            -e "s/%as_addr%/127.0.0.1/g" \
            -e "s,%admpw%,testtmp/admpw,g" \
            -e "s,%as_error%,testtmp/error,g" \
            -e "s,%as_access%,testtmp/access,g" \
            -e "s,%as_pid%,testtmp/pid,g" \
            -e "s,%as_help_path%,testtmp,g" $file > $tmpfile
        ldapmodify -x -h $hostname -p $port -D "$rootdn" -w "$rootpw" -c -a -f $tmpfile
        rm -f $tmpfile
    done
fi

rm -rf testtmp
mkdir testtmp

if [ "$usessl" ] ; then
    ldapurl="ldaps://$hostname:$secport/o=NetscapeRoot"
else
    ldapurl="ldap://$hostname:$port/o=NetscapeRoot"
fi

cat > testtmp/adm.conf <<EOF
ldapurl:    $ldapurl
ldapHost:   $hostname
ldapPort:   $port
sie:   cn=admin-serv-$instance,cn=389 Administration Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot
isie:   cn=389 Administration Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot
port:   9830
ldapStart:   slapd-$instance/start-slapd
securitydir:	$secdir
EOF
cp testtmp/adm.conf testtmp/adm.conf.orig

cat > testtmp/admpw <<EOF
admin:{SHA}0DPiKuNIrrVmD8IUCuw1hQxNqZc=
EOF

cat > testtmp/console.conf <<EOF
Listen $hostname:9830
CustomLog testtmp/access common
ErrorLog testtmp/error
PidFile  testtmp/pid
User $USER
EOF

dir=`pwd`

# CGI env. vars
ADMSERV_CONF_DIR=$dir/testtmp
#ADMSERV_CONF_DIR=$sroot/etc/fedora-ds/admin-serv
export ADMSERV_CONF_DIR
ADMSERV_LOG_DIR=$dir/testtmp
export ADMSERV_LOG_DIR
HTTP_ACCEPT_LANGUAGE=en
export HTTP_ACCEPT_LANGUAGE
SERVER_URL=http://localhost
export SERVER_URL

pwpfile=/tmp/pwp.$$
cat > $pwpfile <<EOF
User: admin
Password: $adminpw

UserDN: uid=admin,ou=Administrators,ou=TopologyManagement,o=NetscapeRoot
SIEPWD: $adminpw
EOF

VALGRIND="valgrind -q --tool=memcheck --leak-check=yes --suppressions=/share/scripts/valgrind.supp --num-callers=40 --log-file="
GDB="gdb -x .gdbinit "
#DEBUGCMD="$VALGRIND"
DEBUGCMD="$GDB"

PROGS="help"
#PROGS="mergeConfig admpw security ugdsconfig ReadLog start_config_ds \
#	config statpingserv viewdata dsconfig monreplication restartsrv \
#	statusping viewlog htmladmin sec-activate stopsrv download help"

#SCRIPTS=ds_create

#$sroot/lib/dirsrv/slapd-$instance/stop-slapd

# each prog has a subdir containing the GET/POST args and any other test data
for prog in $PROGS ; do
    getlist=/tmp/gettests.$$
    find $testdir/$prog -name testget.\* -print 2> /dev/null | sort -n > $getlist
    for test in `cat $getlist` ; do
        if [ ! -d results/$prog ] ; then mkdir -p results/$prog ; fi
        sed -e s/HOSTNAME/$hostname/g -e s/DOMAIN/$domain/g -e s/INSTANCE/$instance/g -e s/PORT/$port/g -e s/USER/$USER/g $test > $test.sed
        basetest=`basename $test`
        echo "Running test $test"
        REQUEST_METHOD=GET ; export REQUEST_METHOD
        QUERY_STRING="`cat $test.sed`" ; export QUERY_STRING
        SCRIPT_NAME=admin-serv/Tasks/Operation/$prog ; export SCRIPT_NAME
	    # open pwpfile for reading as file desc 4 - CGIs have to use stdin (0) for POST
        exec 4<$pwpfile
        PASSWORD_PIPE=4 ; export PASSWORD_PIPE
        if [ "$DEBUGCMD" = "$VALGRIND" ] ; then
            VGFILE="results/$prog/vg.$basetest"
        fi
        if [ -n "$DEBUGCMD" -a "$DEBUGCMD" = "$GDB" ] ; then
            echo "break main" > .gdbinit
            echo "run > results/$prog/$basetest.html" >> .gdbinit
            ./libtool --mode execute $GDB ./$prog
        else
            ./libtool --mode execute ${DEBUGCMD}$VGFILE ./$prog > results/$prog/$basetest.html
        fi
        rm $test.sed
    done
    rm -f $getlist
    cp -p testtmp/adm.conf.orig testtmp/adm.conf
    postlist=/tmp/posttests.$$
    find $testdir/$prog -name testpost.\* -print 2> /dev/null | sort -n > $postlist
    for test in `cat $postlist` ; do
        if [ ! -d results/$prog ] ; then mkdir -p results/$prog ; fi
        sed -e s/HOSTNAME/$hostname/g -e s/DOMAIN/$domain/g -e s/INSTANCE/$instance/g -e s/PORT/$port/g -e s/USER/$USER/g $test > $test.sed
        basetest=`basename $test`
        echo "Running test $test"
        REQUEST_METHOD=POST ; export REQUEST_METHOD
        CONTENT_LENGTH=`wc -c $test.sed | cut -f1 -d' '` ; export CONTENT_LENGTH
        SCRIPT_NAME=admin-serv/Tasks/Operation/$prog ; export SCRIPT_NAME
	    # open pwpfile for reading as file desc 4 - CGIs have to use stdin (0) for POST
        exec 4<$pwpfile
        PASSWORD_PIPE=4 ; export PASSWORD_PIPE
        if [ "$DEBUGCMD" = "$VALGRIND" ] ; then
            VGFILE="results/$prog/vg.$basetest"
        fi
        if [ -n "$DEBUGCMD" -a "$DEBUGCMD" = "$GDB" ] ; then
            echo "break main" > .gdbinit
            echo "run < $test.sed > results/$prog/$basetest.html" >> .gdbinit
            ./libtool --mode execute $GDB ./$prog
        else
            ./libtool --mode execute ${DEBUGCMD}$VGFILE ./$prog < $test.sed > results/$prog/$basetest.html
        fi
        rm $test.sed
    done
    rm -f $postlist
    cp -p testtmp/adm.conf.orig testtmp/adm.conf
done

for prog in $SCRIPTS ; do
    getlist=/tmp/gettests.$$
    find $testdir/$prog -name testget.\* -print 2> /dev/null | sort -n > $getlist
    for test in `cat $getlist` ; do
        if [ ! -d results/$prog ] ; then mkdir -p results/$prog ; fi
        sed -e s/HOSTNAME/$hostname/g -e s/DOMAIN/$domain/g -e s/INSTANCE/$instance/g -e s/PORT/$port/g -e s/USER/$USER/g $test > $test.sed
        basetest=`basename $test`
        echo "Running test $test"
        REQUEST_METHOD=GET ; export REQUEST_METHOD
        QUERY_STRING="`cat $test.sed`" ; export QUERY_STRING
        SCRIPT_NAME=slapd/Tasks/Operation/$prog ; export SCRIPT_NAME
        # open pwpfile for reading as file desc 4 - CGIs have to use stdin (0) for POST
        exec 4<$pwpfile
        PASSWORD_PIPE=4 ; export PASSWORD_PIPE
        if [ -n "$DEBUGCMD" -a "$DEBUGCMD" = "$GDB" ] ; then
            perl -d admserv/cgi-src40/$prog
        else
            perl -w admserv/cgi-src40/$prog
        fi
        rm $test.sed
    done
    rm -f $getlist
    cp -p testtmp/adm.conf.orig testtmp/adm.conf
done

rm -rf $pwpfile .gdbinit
