%define pkgname   dirsrv

Summary:          389 Administration Server (admin)
Name:             389-admin
Version:          1.1.8
Release:          2%{?dist}
License:          GPLv2 and ASL 2.0
URL:              http://port389.org/
Group:            System Environment/Daemons
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:         fedora-ds-admin = %{version}-%{release}
Obsoletes:        fedora-ds-admin < 1.1.8-1

BuildRequires:    nspr-devel
BuildRequires:    nss-devel
BuildRequires:    svrcore-devel
BuildRequires:    mozldap-devel
BuildRequires:    cyrus-sasl-devel
BuildRequires:    icu
BuildRequires:    libicu-devel
BuildRequires:    httpd-devel
BuildRequires:    apr-devel
BuildRequires:    mod_nss
BuildRequires:    389-adminutil-devel

Requires:         389-ds-base
Requires:         mod_nss
# the following are needed for some of our scripts
Requires:         perl-Mozilla-LDAP
Requires:         nss-tools

# for the init script
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig
Requires(preun): /sbin/service

Source0:          http://port389.org/sources/%{name}-%{version}.tar.bz2
# 389-admin-git.sh should be used to generate the source tarball from git
Source1:          %{name}-git.sh

%description
389 Administration Server is an HTTP agent that provides management features
for 389 Directory Server.  It provides some management web apps that can
be used through a web browser.  It provides the authentication, access control,
and CGI utilities used by the console.

%prep
%setup -q -n %{name}-%{version}

%build
%configure --disable-rpath

# Generate symbolic info for debuggers
export XCFLAGS=$RPM_OPT_FLAGS

%ifarch x86_64 ppc64 ia64 s390x sparc64
export USE_64=1
%endif

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT 

make DESTDIR="$RPM_BUILD_ROOT" install

# make console jars directory
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{pkgname}/html/java

#remove libtool and static libs
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/*.so
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/%{pkgname}/modules/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/%{pkgname}/modules/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%pre
# save permission info in temp file
# if upgrading
if [ "$1" -gt 1 ] ; then
    set -x
    mydt=`date +%Y%m%d`
    mytmpfile=${TMPDIR:-/tmp}/$mydt.$PPID
    perm1=`stat --format=%%a %{_sysconfdir}/%{pkgname}/admin-serv 2> /dev/null`
    if [ -z "$perm1" ] ; then
        perm1=0
    fi
    own1=`stat --format=%%U:%%G %{_sysconfdir}/%{pkgname}/admin-serv 2> /dev/null`
    if [ -z "$own1" ] ; then
        own1=0
    fi
    perm2=`stat --format=%%a %{_sysconfdir}/%{pkgname}/admin-serv/console.conf 2> /dev/null`
    if [ -z "$perm2" ] ; then
        perm2=0
    fi
    own2=`stat --format=%%U:%%G %{_sysconfdir}/%{pkgname}/admin-serv/console.conf 2> /dev/null`
    if [ -z "$own2" ] ; then
        own2=0
    fi
    echo $perm1 $own1 $perm2 $own2 > $mytmpfile
fi

%post
/sbin/chkconfig --add %{pkgname}-admin
/sbin/ldconfig
# if installing for the first time, make sure the config
# directory and file have the correct permissions
# the setup script is used to "fix" these
if [ "$1" -gt 1 ]; then
    set -x
    mydt=`date +%Y%m%d`
    mytmpfile=${TMPDIR:-/tmp}/$mydt.$PPID
    if [ ! -f $mytmpfile ] ; then
        echo Error: upgrade temp file $mytmpfile not found
        exit 1
    fi
    cat $mytmpfile | while read perm1 own1 perm2 own2 ; do
        chmod $perm1 %{_sysconfdir}/%{pkgname}/admin-serv
        chown $own1 %{_sysconfdir}/%{pkgname}/admin-serv
        chmod $perm2 %{_sysconfdir}/%{pkgname}/admin-serv/console.conf
        chown $own2 %{_sysconfdir}/%{pkgname}/admin-serv/console.conf
    done
    rm -f $mytmpfile
fi

%preun
if [ $1 = 0 ]; then
        /sbin/service %{pkgname}-admin stop >/dev/null 2>&1 || :
        /sbin/chkconfig --del %{pkgname}-admin
fi

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE
%dir %{_sysconfdir}/%{pkgname}/admin-serv
%config(noreplace)%{_sysconfdir}/%{pkgname}/admin-serv/*.conf
%{_datadir}/%{pkgname}
%{_sysconfdir}/rc.d/init.d/%{pkgname}-admin
%config(noreplace)%{_sysconfdir}/sysconfig/%{pkgname}-admin
%{_sbindir}/*
%{_libdir}/*.so.*
%{_libdir}/%{pkgname}
%{_mandir}/man8/*

%changelog
* Tue Jul 21 2009 Rich Megginson <rmeggins@redhat.com> - 1.1.8-2
- use 389-adminutil instead of adminutil

* Thu Jun 18 2009 Rich Megginson <rmeggins@redhat.com> - 1.1.8-1
- bump version to 1.1.8
- change license to GPLv2 + ASL 2.0
- changed files that were incorrectly licensed as GPLv2+ to plain GPLv2

* Wed Apr  8 2009 Rich Megginson <rmeggins@redhat.com> - 1.1.7-3
- Resolves: bug 494980
- Description: setup-ds-admin.pl -u and silent setup complain about ServerIpAddress
- CVS tag FedoraDirSrvAdmin_1_1_7_RC3 FedoraDirSrvAdmin_1_1_7_RC3_20090408

* Fri Apr  3 2009 Rich Megginson <rmeggins@redhat.com> - 1.1.7-2
- Resolves: bug 493989
- Description: Admin Server: valgrind invalid read in security.c when installing CRL

* Tue Mar 31 2009 Rich Megginson <rmeggins@redhat.com> - 1.1.7-1
- this is the 1.1.7 release
- added man pages for setup, migration, remove commands
- better error handling for command line utilities
- fixed remove from console
- added remove-ds-admin.pl
- added pre and post sections in order to preserve the permissions and ownerships
- CVS tag FedoraDirSrvAdmin_1_1_7_RC1 FedoraDirSrvAdmin_1_1_7_RC1_20090331

* Mon Jul 14 2008 Rich Megginson <rmeggins@redhat.com> - 1.1.6-1
- This is for the fedora ds 1.1.2 release

* Wed Jan  9 2008 Rich Megginson <rmeggins@redhat.com> - 1.1.2-1
- Fix issues associated with Fedora pkg review bug 249548

* Tue Dec 11 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.1-1
- this is the final GA candidate

* Tue Nov  6 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.16
- fix several beta blocker issues

* Mon Oct 15 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.15
- fix bogus dist macro
- change mozldap6 to mozldap

* Thu Oct 11 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.14
- make admin server work with SELinux enabled
- fix wording errors in setup

* Mon Oct  8 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.13
- added /etc/sysconfig/dirsrv-admin the file that allows you to set
- the environment used to start up the admin server (e.g. keytab, ulimit, etc.)
- the initscript and admin start script use this file now
- This version also has a fix to print the correct error message if the admin
- server cannot be contacted during setup or migration.

* Thu Sep 27 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.12
- fix a couple of migration issues, including the rpath $libdir problem
- allow ds_remove from console to remove instances

* Wed Sep 19 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.11
- one line fix to fix of 295001 - console.conf clobbered

* Tue Sep 18 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.10
- fixed migration issue bugzilla 295001 - console.conf clobbered

* Fri Sep 14 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.9
- fix several more migration problems

* Fri Sep 14 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.8
- fix migration - servers are started as they are migrated now

* Tue Aug 21 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.7
- Fix the with-fhs-opt configure flag

* Fri Aug 17 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.6
- remove curses
- make mod_admserv link against sasl
- add the usual .m4 files to mod_admserv instead of having all of
- the component logic in configure.in

* Thu Aug 16 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.5
- incorporate Noriko's migration fix

* Wed Aug 15 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.4
- address several migration issues

* Mon Aug 13 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.3
- there is no devel package, so remove unused .so files

* Mon Aug 13 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.2
- forgot to tag the modules

* Fri Aug 10 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1.1
- get rid of cvsdate
- use pkgname of dirsrv for filesystem path naming
- get rid of devel package
- simplify files section

* Fri Aug 10 2007 Noriko Hosoi <nhosoi@redhat.com> - 1.1.0-0.3.20070810
- updated to latest sources
- upgraded the mozldap6 version to 6.0.4

* Wed Aug  8 2007 Noriko Hosoi <nhosoi@redhat.com> - 1.1.0-0.2.20070808
- updated to latest sources -- bug fixes in the setup scripts

* Mon Aug  6 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-0.1.20070806
- updated to latest sources

* Thu Aug  2 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-0.4.20070802
- There are no files in bindir anymore

* Thu Aug  2 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-0.3.20070802
- forgot to prepend build root to java dir creation

* Thu Aug  2 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-0.2.20070802
- forgot to add mod_admserv and mod_restartd to source

* Thu Aug  2 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-0.1.20070802
- updated to latest sources - fix build breakage
- add console jars dir under html

* Mon Jul 23 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-0.1.20070725
- Initial version based on fedora-ds-base.spec
