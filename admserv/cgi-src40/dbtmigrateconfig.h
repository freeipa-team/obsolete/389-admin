/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
 
#define CGI_NAME "migrateConfig"
 
static char dbtmigrateConfigid[] = "$DBT: migrateConfig referenced v1 $";
 
#include "libadminutil/resource.h"

BEGIN_STR(migrateConfig)
  /* external string used in migrateConfig.c */
  ResDef( DBT_CGIID_, -1, dbtmigrateConfigid )
  ResDef( DBT_NO_METHOD, 1, "No method is specified" )
  ResDef( DBT_UNKNOWN_METHOD, 2, "Unknown Method (%s)" )
  ResDef( DBT_NO_QUERY_STRING, 3, "No data in QUERY_STRING environment variable" )
  ResDef( DBT_ERROR_OPEN_FILE, 4, "Error open file: %s" )
  ResDef( DBT_ERROR_READ_FILE, 5, "Error readiong file: %s" )
  ResDef( DBT_ERROR_OPEN_DIR, 6, "Error open directory: %s" )
  ResDef( DBT_DBT_NO_OLD_SERVER_ROOT, 7, "No old server root specified" )
  ResDef( DBT_NO_SERVER_FOUND, 8, "No server found" )
  ResDef( DBT_NO_USER_NAME, 9, "No user name is specified" )
  ResDef( DBT_NO_USER_DN, 10, "No user distinguished name is specified" )
  ResDef( DBT_PSET_CREATE_ERROR, 11, "PSET Creation Failed" )
  ResDef( DBT_PSET_SET_ERROR, 12, "PSET set operation failed" )
  ResDef( DBT_PSET_ADD_ERROR, 13, "PSET add operation failed" )
  ResDef( DBT_SSL_INIT_ERROR, 14, "SSL related initialization failed" )
END_STR(migrateConfig)
