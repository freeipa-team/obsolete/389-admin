/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#define LIBRARY_NAME "cgiadmin"

static char dbtcgiadminid[] = "$DBT: cgiadmin referenced v1 $";

#include "i18n.h"

BEGIN_STR(cgiadmin)
	ResDef( DBT_LibraryID_, -1, dbtcgiadminid )/* extracted from dbtcgiadmin.h*/
	ResDef( DBT_missingRequestMethod_, 1, "Missing REQUEST_METHOD" )/*extracted from cladd.c*/
	ResDef( DBT_thisShouldOnlyBeInvokedAsCgiProg_, 2, "This should only be invoked as CGI program" )/*extracted from cladd.c*/
	ResDef( DBT_missingAdmservRoot_, 3, "Missing ADMSERV_ROOT" )/*extracted from cladd.c*/
	ResDef( DBT_thisShouldOnlyBeInvokedAsCgiProg_1, 4, "This should only be invoked as CGI program" )/*extracted from cladd.c*/
	ResDef( DBT_unrecognizedRequestType_, 5, "Unrecognized request type." )/*extracted from cladd.c*/
	ResDef( DBT_startup_, 6, "Startup" )/*extracted from cladd.c*/
	ResDef( DBT_failedToInitializeWinsock_, 7, "Failed to initialize WinSock" )/*extracted from cladd.c*/
	ResDef( DBT_badFile_, 8, "Bad file" )/*extracted from cladd.c*/
	ResDef( DBT_expectedServersLst_, 9, "Expected servers.lst" )/*extracted from cladd.c*/
	ResDef( DBT_clusterMergeProductLstFromS_, 10, "Cluster: merge product.lst from %s" )/*extracted from cladd.c*/
	ResDef( DBT_IObtainingProductInformationFrom_, 11, "<i>Obtaining product information from %s://%s:%d</i><br>\n" )/*extracted from cladd.c*/
	ResDef( DBT_updatedSBrN_, 12, "Updated %s<BR>\n" )/*extracted from cladd.c*/
	ResDef( DBT_badFile_1, 13, "Bad file" )/*extracted from cladd.c*/
	ResDef( DBT_expectedServersInstanceLst_, 14, "Expected servers-instance.lst" )/*extracted from cladd.c*/
	ResDef( DBT_warningSAlreadyExistsInformation_, 15, "Warning: %s already exists, information not added<BR>\n" )/*extracted from cladd.c*/
	ResDef( DBT_errorFailedToCreateFileSBrN_, 16, "Error: failed to create file %s<BR>\n" )/*extracted from cladd.c*/
	ResDef( DBT_warningDirectorySWillBeRemovedBe_, 17, "Warning: directory %s will be removed because of above error<BR>\n" )/*extracted from cladd.c*/
	ResDef( DBT_errorSSErrnoDBrN_, 19, "Error: %s %s (errno = %d)<BR>\n" )/*extracted from cladd.c*/
	ResDef( DBT_errorSSSBrN_, 20, "Error: %s %s, %s<BR>\n" )/*extracted from cladd.c*/
	ResDef( DBT_allowAllOperations_, 21, "ALLOW ALL OPERATIONS" )/*extracted from snmpcomm.c*/
	ResDef( DBT_allowGetOperations_, 22, "ALLOW GET OPERATIONS" )/*extracted from snmpcomm.c*/
	ResDef( DBT_allowSetOperations_, 23, "ALLOW SET OPERATIONS" )/*extracted from snmpcomm.c*/
	ResDef( DBT_youAccessedThisFormWithAnInvalid_, 24, "You accessed this form with an invalid query string." )/*extracted from snmpcomm.c*/
	ResDef( DBT_communityHasBeenRemoved_, 25, "community has been removed." )/*extracted from snmpcomm.c*/
	ResDef( DBT_communityStrings_, 26, "Community Strings" )/*extracted from snmpcomm.c*/
	ResDef( DBT_editACommunity_, 27, "Edit a Community" )/*extracted from snmpcomm.c*/
	ResDef( DBT_addAnotherCommunity_, 28, "Add Another Community" )/*extracted from snmpcomm.c*/
	ResDef( DBT_edit_, 30, "Edit" )/*extracted from snmpcomm.c*/
	ResDef( DBT_remove_, 31, "Remove" )/*extracted from snmpcomm.c*/
	ResDef( DBT_doYouReallyWantToRemoveThisEntry_, 32, "Do you really want to remove this entry?" )/*extracted from snmpcomm.c*/
	ResDef( DBT_BCommunityBSN_, 33, "<b>Community:</b> %s\n" )/*extracted from snmpcomm.c*/
	ResDef( DBT_BrBOperationBSN_, 34, "<br><b>Operation:</b> %s\n" )/*extracted from snmpcomm.c*/
	ResDef( DBT_noCommunitiesExist_, 35, "No communities exist." )/*extracted from snmpcomm.c*/
	ResDef( DBT_youShouldEnterACommunityString_, 36, "You should enter a community string." )/*extracted from snmpcomm.c*/
	ResDef( DBT_youShouldEnterOperationYouWantFo_, 37, "You should enter operation you want for this community." )/*extracted from snmpcomm.c*/
	ResDef( DBT_theCommunityEntryHasBeenChanged_, 38, "the community entry has been changed" )/*extracted from snmpcomm.c*/
	ResDef( DBT_SHasBeenAdded_, 39, "%s has been added" )/*extracted from snmpcomm.c*/
	ResDef( DBT_failsToStartUpMasterAgent_, 40, "fails to start up master agent" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsToStartIt_, 41, "Please refer to documents to start it" )/*extracted from snmpcomm.c*/
	ResDef( DBT_masterAgentStartUp_, 42, "master agent start up" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTOpenConfigFile_, 43, "can't open config file" )/*extracted from snmpcomm.c*/
	ResDef( DBT_openTempFileFails_, 44, "open temp file fails" )/*extracted from snmpcomm.c*/
	ResDef( DBT_openConfigFails_, 45, "open CONFIG fails" )/*extracted from snmpcomm.c*/
	ResDef( DBT_openConfigFails_1, 46, "open CONFIG fails" )/*extracted from snmpcomm.c*/
	ResDef( DBT_noPermissionToStartMasterAgent_, 47, "No permission to start master agent" )/*extracted from snmpcomm.c*/
	ResDef( DBT_youMustBeRunningAsSuperUserPleas_, 48, "You must be running as super user. Please refer to documents" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTGetTcpProtocolEntryN_, 49, "can't get tcp protocol entry\n" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTCreateASockect_, 50, "can't create a sockect" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsForItsConf_, 51, "Please refer to documents for its configuration" )/*extracted from snmpcomm.c*/
	ResDef( DBT_aSmuxMasterAgentIsRunningOrTheSm_, 52, "a smux master agent is running or the smux port is not free yet" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsForItsConf_1, 53, "Please refer to documents for its configuration" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTGetUdpProtocolEntryN_, 54, "can't get udp protocol entry\n" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTCreateASocket_, 55, "can't create a socket" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsForItsConf_2, 56, "Please refer to documents for its configuration" )/*extracted from snmpcomm.c*/
	ResDef( DBT_aSnmpdIsRunningOrSnmpPortIsNotFr_, 57, "a snmpd is running or snmp port is not free yet" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsForItsConf_3, 58, "Please refer to documents for its configuration" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTGetUdpProtocolEntryN_1, 59, "can't get udp protocol entry\n" )/*extracted from snmpcomm.c*/
	ResDef( DBT_canTCreateASocket_1, 60, "can't create a socket" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsForItsConf_4, 61, "Please refer to documents for its configuration" )/*extracted from snmpcomm.c*/
	ResDef( DBT_failToStart_, 62, "Fail to start" )/*extracted from snmpcomm.c*/
	ResDef( DBT_pleaseReferToDocumentsForItsConf_5, 63, "Please refer to documents for its configuration" )/*extracted from snmpcomm.c*/
END_STR(cgiadmin)
