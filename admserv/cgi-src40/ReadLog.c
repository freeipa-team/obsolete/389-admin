/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/


/** * CGI Browser test syntax: * *
http://host:port/admin-serv/bin/readlog?op=read&name=filename&start=linenum&stop=linenum
* Returns log entries in filename, start and stop are optional * *
http://host:port/admin-serv/bin/readlog?op=count&name=filename * Returns
number of log entries * * @date 11/29/97 * @author ahakim@netscape.com */
#include "libadmin/libadmin.h" 
#include "libadminutil/admutil.h" 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include "nspr.h"

#ifndef BIG_LINE
#define BIG_LINE 512
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

char errorlog[BIG_LINE];
char accesslog[BIG_LINE];

int readLogFile(char *filename, int startEntry, int endEntry)
{
	char line[BIG_LINE];
	int entryIndex = 0;

	FILE *f = fopen(filename, "r");

	if(f != NULL)
	{
		while(fgets(line, BIG_LINE, f))
		{
			if(strstr(line, "format=") == NULL)
			{
				if((entryIndex >= startEntry) && (entryIndex <= endEntry))
					fprintf(stdout, "%s", line);
				entryIndex++;
			}
		}
		fclose(f);
		return 0;
	}
	return 1;
}


char *getLogName(char *line)
{
	char *log;
	char *eol;

	log = strrchr(line, ':');
	log++;
	log++;
	eol = strchr(line, '\n');
	eol[0] = '\0';
	return log;
}


int getLogNames()
{
    char line[BIG_LINE];
    const char *configdir = util_get_conf_dir();
    char filename[BIG_LINE];
    FILE *f;

    if (!util_find_file_in_paths(filename, sizeof(filename), "local.conf",
                                 configdir, "", "admin-serv/config")) {
        return 0;
    }

	f  = fopen(filename, "r");

	if(f)
	{
		while(!feof(f))
		{
			fgets(line, sizeof(line), f);

			if(PL_strcasestr(line, "nserrorlog"))
			{
				PR_snprintf(errorlog, sizeof(errorlog), "%s", getLogName(line));
			}
			else
			if(PL_strcasestr(line, "nsaccesslog"))
			{
				PR_snprintf(accesslog, sizeof(accesslog), "%s", getLogName(line));
			}
		}
		fclose(f);
	}
	return 0;
}


int getEntryCount(char *filename, int *value)
{
	char line[BIG_LINE];
	int entryIndex = 0;

	FILE *f = fopen(filename, "r");

	if(f != NULL)
	{
		while(fgets(line, BIG_LINE, f) != NULL)
		{
			if(strstr(line, "format=") == NULL)
				entryIndex++;
		}
		*value = entryIndex;
		fclose(f);
		return 0;
	}	
	return 1;
}


int main(int argc, char *argv[])
{
	char *qs = NULL;
	char *method = NULL;
	char *operation = NULL;
	int proceed = FALSE;

    fprintf(stdout, "Content-type: text/html\n\n");
    /*fprintf(stdout, "Number of Entries = %d\n\n", getEntryCount("error"));*/

	method = getenv("REQUEST_METHOD");
	if(method)
	{
		if(!strcmp(method, "GET"))
		{
			qs = getenv("QUERY_STRING");
			if(qs && *qs)
			{
				get_begin(qs);
				proceed = TRUE;
			}
		}
		else
		if(!strcmp(method, "POST")) 
		{
			post_begin(stdin);
			proceed = TRUE;
		}
	}

	if(!proceed)
	{
		fprintf(stdout, "Status: 1\nErrorString: REQUEST_METHOD=%s, QUERY_STRING=%s\n", method, qs);
		exit(1);
	}

	proceed=FALSE;
	operation = (char *)get_cgi_var("op", NULL, NULL);
	if(operation)
	{
		char *name = NULL;
		char *start = NULL;
		char *stop = NULL;

		getLogNames();

		if(!strcmp(operation, "read"))
		{
			name = (char *)get_cgi_var("name", NULL, NULL);
			start = (char *)get_cgi_var("start", NULL, NULL);
			stop = (char *)get_cgi_var("stop", NULL, NULL);
			if(name)
			{
				if(strstr(name, "access"))
					name = accesslog;
				else
					name = errorlog;

				if(readLogFile(name, start?atoi(start):0, stop?atoi(stop):99999) == 0)
					proceed = TRUE;
			}

			if(!proceed)
			{
				fprintf(stdout, "Status: 1\nErrorString: name=%s\n", name?name:"");
				exit(1);
			}
		}

		if(!strcmp(operation, "count"))
		{
			name = (char *)get_cgi_var("name", NULL, NULL);
			if(name)
			{
				int value;

				if(strstr(name, "access"))
					name = accesslog;
				else
					name = errorlog;

				if(getEntryCount(name, &value) == 0)
				{
					proceed = TRUE;
					fprintf(stdout, "count=%d\n", value);
				}
			}

			if(!proceed)
			{
				fprintf(stdout, "Status: 1\nErrorString: name=%s\n", name?name:"");
				exit(1);
			}
		}

	}

	if(!proceed)
	{
		fprintf(stdout, "Status: 1\nErrorString: op=%s\n", operation?operation:"");
		exit(1);
	}

	return 0;
}




