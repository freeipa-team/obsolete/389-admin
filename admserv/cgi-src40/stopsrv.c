/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * stopadm.c:  Kills the admin server.
 *
 * Mike McCool
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <prtypes.h>

#include "libadminutil/distadm.h"
#include "libadminutil/admutil.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#ifdef XP_UNIX
#include <unistd.h>
#endif
#include "libadminutil/resource.h"

#include "libadmin/libadmin.h"

#define RESOURCE_FILE "stopsrv"

#define resource_key(a,b)   a b

#define DBT_NO_NEW_PROCESS 			resource_key(RESOURCE_FILE, "1")
#define DBT_SHUTDOWN_ACK 		        resource_key(RESOURCE_FILE, "2")
#define DBT_CANT_STOP_SRV 	                resource_key(RESOURCE_FILE, "3")
#define DBT_EXPRESS_SUCCESS 		        resource_key(RESOURCE_FILE, "4")
#define DBT_EXPRESS_FAILURE                     resource_key(RESOURCE_FILE, "5")
#define DBT_SHUTDOWN_EXPRESS_WARNING 		resource_key(RESOURCE_FILE, "6")



#define DBT_UNIX_I18N_STOP_FAIL           resource_key(RESOURCE_FILE, "20")
#define DBT_UNIX_STOP_FAIL                resource_key(RESOURCE_FILE, "21")
#define DBT_UNIX_I18N_STOP_SUCCESS        resource_key(RESOURCE_FILE, "22")
#define DBT_UNIX_STOP_SUCCESS             resource_key(RESOURCE_FILE, "23")
#define DBT_WIN32_I18N_STOP_SUCCESS       resource_key(RESOURCE_FILE, "24")
#define DBT_WIN32__STOP_SUCCESS           resource_key(RESOURCE_FILE, "25")

char           *acceptLanguage = "en";
Resource       *i18nResource = NULL;

char*
getResourceString(char *key) {
  return (char *)(res_getstring(i18nResource, key, acceptLanguage, NULL, 0, NULL));
}

/* html resource string */
int main(int argc, char *argv[])
{
  int _ai=ADMUTIL_Init();
  char *m = getenv("REQUEST_METHOD");
  char msgbuf[BUFSIZ];
  char line[BIG_LINE];
  char *lang=getenv("HTTP_ACCEPT_LANGUAGE");

  char           *return_format = NULL;
  char           *qs = 0;

  (void)_ai; /* get rid of unused variable warning */
  i18nResource = res_find_and_init_resource(PROPERTYDIR, RESOURCE_FILE);

  if (lang) acceptLanguage = strdup(lang);

  fprintf(stdout, "Content-type: text/html\n\n");

  if(!strcmp(m, "GET")) {
    qs = getenv("QUERY_STRING");
    if(qs) {
      get_begin(qs);
      return_format = get_cgi_var("return_format", NULL, NULL);
    }
  }
  else
    post_begin(stdin);

  

#ifdef XP_UNIX
  switch(fork()) {
  case -1:
    if (i18nResource) {
      if(return_format && !strcmp(return_format, "html")) {
	fprintf(stdout, (const char*) getResourceString(DBT_UNIX_I18N_STOP_FAIL), getResourceString(DBT_EXPRESS_FAILURE), getResourceString(DBT_NO_NEW_PROCESS));
      }
      else {
	rpt_err(SYSTEM_ERROR, 
		res_getstring(i18nResource, 
			      DBT_NO_NEW_PROCESS, 
			      acceptLanguage, msgbuf, sizeof(msgbuf), NULL),
		NULL, NULL);
      }
    }
    else {
      if(return_format && !strcmp(return_format, "html")) {
	fprintf(stdout, "%s", getResourceString(DBT_UNIX_STOP_FAIL));
      }
      else {
	rpt_err(SYSTEM_ERROR,
		"Couldn't create a new process to stop the Administration Server.",
		NULL, NULL);
      }
    }
    break;
  case 0:
    fclose(stdin);
    fclose(stdout);
    fclose(stderr);
    if(setsid() == -1) {
      exit(0);
    }
    /* wait for a bit so the server can give */
    /* feedback */
#ifdef AIX
    PR_Sleep(5); /* AIX doesn't like sleep(5) */
#else
    sleep(5);
#endif
#if defined(ENABLE_SERVICE)
    PR_snprintf(line, sizeof(line), "service " PACKAGE_NAME " stop");
    system(line);
#else
    if (util_find_file_in_paths(line,  sizeof(line), "stop-ds-admin", CMDBINDIR, "../..", "")) {
        system(line);
    }
#endif
    exit(0);
    break;
  default:
    /* comment out temporily, should be back later
    log_change(TO_ADMIN, "Admin server shutdown on user request");
    */
    /* Child process will notify client */
    if (i18nResource) {
      if(return_format && !strcmp(return_format, "html")) {
	fprintf(stdout, (const char*)getResourceString(DBT_UNIX_I18N_STOP_SUCCESS), getResourceString(DBT_EXPRESS_SUCCESS), getResourceString(DBT_SHUTDOWN_EXPRESS_WARNING));
      }
      else {
	rpt_unknown(res_getstring(i18nResource, 
				  DBT_SHUTDOWN_ACK, 
				  acceptLanguage, msgbuf, sizeof(msgbuf), NULL));
      }
    }
    else {
      if(return_format && !strcmp(return_format, "html")) {
	fprintf(stdout, "%s", getResourceString(DBT_UNIX_STOP_SUCCESS));
      }
      else {
	rpt_unknown("Admin server should be shutdown on user request");
      }
    }
    fflush(stdout);
    break;
  }
  return 0;	
#else /* XP_WIN32 */
  if (i18nResource) {
    if(return_format && !strcmp(return_format, "html")) {
      fprintf(stdout, (const char*)getResourceString(DBT_WIN32_I18N_STOP_SUCCESS), getResourceString(DBT_EXPRESS_SUCCESS), getResourceString(DBT_SHUTDOWN_EXPRESS_WARNING));
    }
    else {
      rpt_unknown(res_getstring(i18nResource, 
				DBT_SHUTDOWN_ACK, 
				acceptLanguage, msgbuf, sizeof(msgbuf), NULL));
    }
  }
  else {
    if(return_format && !strcmp(return_format, "html")) {
      fprintf(stdout, getResourceString(DBT_WIN32__STOP_SUCCESS));
    }
    else {
      rpt_unknown("Admin server should be shutdown on user request");
    }
  }
  fflush(stdout);
  {
	STARTUPINFO sui;
	PROCESS_INFORMATION pi;

	sui.cb               = sizeof(STARTUPINFO);
	sui.lpReserved       = 0;
	sui.lpDesktop        = NULL;
	sui.lpTitle          = NULL;
	sui.dwX              = 0;
	sui.dwY              = 0;
	sui.dwXSize          = 0;
	sui.dwYSize          = 0;
	sui.dwXCountChars    = 0;
	sui.dwYCountChars    = 0;
	sui.dwFillAttribute  = 0;
	sui.dwFlags          = STARTF_USESHOWWINDOW;
	sui.wShowWindow      = SW_HIDE;
	sui.cbReserved2      = 0;
	sui.lpReserved2      = 0;
	sui.hStdInput        = 0;
	sui.hStdOutput       = 0;
	sui.hStdError        = 0;

	if(CreateProcess(NULL, "NTStopService.exe", NULL, NULL, FALSE, DETACHED_PROCESS | CREATE_NEW_PROCESS_GROUP, NULL, NULL, &sui, &pi) > 0)
	  return 0; /* success */
  }

  return 1;	
#endif /* XP_WIN32 */
}
