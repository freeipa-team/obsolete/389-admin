/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *

 * END COPYRIGHT BLOCK **/
/*
 * monreplication.c:  Generate form for replication monitoring
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <prtypes.h>

#include "libadminutil/resource.h"
#include "libadminutil/distadm.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include "libadmin/libadmin.h"

#define MY_PAGE "monreplication.html"
#define DEF_SIZE "25"

/*
 * i18n conversions defines and function;
 * properties file  = "monreplication.properties"
 */
#define RESOURCE_FILE "monreplication"
#define resource_key(a,b)   a b

#define DBT_REFRESH_URL		resource_key(RESOURCE_FILE, "1")
#define DBT_REFRESH_INTERVAL	resource_key(RESOURCE_FILE, "2")
#define DBT_CONFIG_FILE		resource_key(RESOURCE_FILE, "3")

static  char *acceptLanguage = (char*)"en";
static  Resource *i18nResource = NULL;

char*
getResourceString(char *key) {
	static char msgbuf[BUFSIZ]; /* ok - not threaded code */
	return (char *)(res_getstring(i18nResource, key, acceptLanguage,
				      msgbuf, sizeof(msgbuf), NULL));
}

static void i18nInit() {
	i18nResource = res_find_and_init_resource(PROPERTYDIR, RESOURCE_FILE);

	if (getenv("HTTP_ACCEPT_LANGUAGE")) {
		acceptLanguage = getenv("HTTP_ACCEPT_LANGUAGE");
	}
}


int main(int argc, char *argv[])
{
	int _ai = ADMUTIL_Init();
	char *qs = getenv("QUERY_STRING");
	char line[BIG_LINE];
	FILE *html = open_html_file(MY_PAGE);
	char *host = NULL;
	char *port = NULL;
	char *admurl = NULL;
	char refreshurl[256] = {'\0'};
	char refreshinterval[16];
	char configfile[256] = {'\0'};
	char *resstr;

	(void)_ai; /* get rid of unused variable warning */
	i18nInit();

	fprintf(stdout, "Content-type: text/html;charset=utf-8\n\n");

	if(qs)  {
		get_begin(qs);
		host = get_cgi_var("servhost", NULL, NULL);
		port = get_cgi_var("servport", NULL, NULL);
		admurl = get_cgi_var("admurl", NULL, NULL);
	}

	
	if ((resstr = getResourceString(DBT_REFRESH_URL))) {
		PR_snprintf(refreshurl, sizeof(refreshurl), resstr, admurl ? admurl : "");
	}
	if ((resstr = getResourceString(DBT_REFRESH_INTERVAL))) {
		PR_snprintf(refreshinterval, sizeof(refreshinterval), resstr);
	}
	else {
		PR_snprintf(refreshinterval, sizeof(refreshinterval), "300");
	}
	if ((resstr = getResourceString(DBT_CONFIG_FILE))) {
		PR_snprintf(configfile, sizeof(configfile), resstr);
	}

	while(next_html_line(html, line))  {
		if(parse_line(line, NULL))  {
			if(directive_is(line, "SERVHOST"))  {
				output_input("hidden", "servhost", host, NULL);
			}
			else if(directive_is(line, "SERVPORT"))  {
				output_input("hidden", "servport", port, NULL);
			}
			else if(directive_is(line, "ADMURL"))  {
				output_input("hidden", "admurl", refreshurl, NULL);
			}
			else if(directive_is(line, "CONFIGFILE"))  {
				output_input("text", "configfile", configfile, "size=80");
			}
			else if(directive_is(line, "REFRESHINTERVAL"))  {
				output_input("text", "refreshinterval", refreshinterval, NULL);
			}
			else
		    		fputs(line, stdout);
		}  
	}

	return 0;	
}
