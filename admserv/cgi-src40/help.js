// BEGIN COPYRIGHT BLOCK
// Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
// Copyright (C) 2005 Red Hat, Inc.
// All rights reserved.

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; version 2
// of the License.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// END COPYRIGHT BLOCK
//
// Help Editor Prototype
// 10/7/97
// David Tompkins
// dt@netscape.com
//
// Utility help functions
//
   
manualBaseURL = document.location.protocol + "//" + document.location.host + "/";

function
manualBase()
{
   return manualBaseURL+"manual";
}

function
help(helpdir, token)
{
   newlocation = manualBase()+"/help/" + helpCommand() + "?helpdir="+helpdir+"&token="+token+"&mapfile=tokens.map";

   if (top.HelpWindow)
   {
      top.HelpWindow.focus();
      top.HelpWindow.location = newlocation;
      return;
   }

   window.open(newlocation, 'helpwin', 'resizable=1,width=640,height=480');
}

function
helpCommand()
{
   return "help";
}

function
findDirectory()
{
   return parent.HelpDirectory;
}

function
back()
{
   parent.frames.HelpContent.history.back();
}

function
forward()
{
   parent.frames.HelpContent.history.forward();
}

function
dismiss()
{
   parent.close();
}

function
goTOC()
{
   parent.frames.HelpContent.location = manualBase()+"/"+findDirectory()+"/help/contents.htm";
}

function
goIndex()
{
   parent.frames.HelpContent.location = manualBase()+"/"+findDirectory()+"/help/index.htm";
}

function
goLibrary()
{
   parent.frames.HelpContent.location = manualBase()+"/help/" + helpCommand() + "?genlib";
}

function
print()
{
   parent.frames.HelpContent.print();
}
