/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*///////////////////////////////////////////////////////*/
/*/                                                     /*/
/*/ David Tompkins (mailto:dt@netscape.com)             /*/
/*/ Netscape Communications Corp.                       /*/
/*/ 9/21/98                                             /*/
/*/                                                     /*/
/*/ start_config_ds: starts a config DS instance        /*/
/*/                                                     /*/
/*///////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <config.h>

#include <libadminutil/admutil.h>
#include <libadmin/libadmin.h>

int
error_exit(char *msg)
{
   printf("Content-type: text/html\n\n");
   printf("<title>StartConfigDS Error</title><h1>StartConfigDS Error</h1>");
   printf("Your request could not be fulfilled.<br><br>");
   printf("<b>Reason :</b> %s<br>", msg);
   exit(0) ;
}

int
success_exit()
{
   sleep(10);
   printf("Content-type: text/html\n");
   printf("Content-length: 13\n\n");
   printf("NMC_Status: 0");
   fflush(stdout);
   fclose(stdout);
   exit(0) ;
}

int
main(int argc, char *argv[])
{
   int _ai = ADMUTIL_Init();
   int   ret_val = 0;
   char *startcmd = 0;
   AdmldapInfo info;
   char *method = NULL;
   char *redir = NULL;
   char *configdir = util_get_conf_dir();

   (void)_ai; /* get rid of unused variable warning */
   method = getenv("REQUEST_METHOD");
   /* find and open the AS config file adm.conf */
   info = admldapBuildInfoOnly(configdir, &ret_val);

   if (!info) {
      return error_exit("Failed to open adm.conf");
   }

   startcmd = admldapGetLdapStart(info);
   if (startcmd) {
      if ((ret_val = system(startcmd)) == -1)
         return error_exit("system() returned -1");

      if (ret_val != 0)
	 return error_exit("Execution of the StartConfigDS command returned non-zero");

      if (method && !strcmp(method, "POST") && !post_begin(stdin) &&
          (redir = get_cgi_var("redir_to", NULL, NULL))) {
         printf("Content-Type: text/html\nLocation: %s\n\n", redir);
         exit(0);
      }

      return success_exit(); 
   }

   return error_exit("No ldapStart entry found in adm.conf");
}
