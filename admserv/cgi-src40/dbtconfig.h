/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
 
#define CGI_NAME "config"
 
static char dbtconfigid[] = "$DBT: config referenced v1 $";
 
#include "libadminutil/resource.h"

BEGIN_STR(config)
  /* external string used in config.c */
  ResDef( DBT_CGIID_, -1, dbtcgiconfigid )
  ResDef( DBT_NO_METHOD, 1, "No method is specified" )
  ResDef( DBT_UNKNOWN_METHOD, 2, "Unknown Method (%s)" )
  ResDef( DBT_NO_QUERY_STRING, 3, "No data in QUERY_STRING environment variable" )
  ResDef( DBT_NO_USER_NAME, 4, "No user name is specified" )
  ResDef( DBT_NO_USER_DN, 5, "No user distinguished name is specified" )
  ResDef( DBT_PSET_CREATE_ERROR, 6, "PSET Creation Failed" )
  ResDef( DBT_NO_OP, 7, "No operation is defined" )
  ResDef( DBT_ILLEGAL_OP, 8, "Illegal operation is defined" )
  ResDef( DBT_NO_ATTRS, 9, "No attribute specified" )
  ResDef( DBT_PSET_GET_ERROR, 10, "PSET get operation failed" )
  ResDef( DBT_PSET_PARTIAL_GET, 11, "PSET get partial results" )
  ResDef( DBT_ATTR_NOT_EXIST, 12, "Attribute(s) does not exist" )
  ResDef( DBT_ATTR_NO_VALUE, 13, "Attribute[%s] does not have value" )
  ResDef( DBT_PSET_SET_ERROR, 14, "PSET set operation failed" )
  ResDef( DBT_PSET_ADD_ERROR, 15, "PSET add operation failed" )
  ResDef( DBT_PSET_SET_NOT_EXIST, 16, "PSET set operation try to set non-exist attribute" )
  ResDef( DBT_SSL_INIT_ERROR, 17, "SSL related initialization failed" )
END_STR(config)
