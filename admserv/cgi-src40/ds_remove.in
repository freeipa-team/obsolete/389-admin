#!@perlexec@
# BEGIN COPYRIGHT BLOCK
# This Program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; version 2 of the License.
# 
# This Program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA.
# 
# Copyright (C) 2007 Red Hat, Inc.
# All rights reserved.
# END COPYRIGHT BLOCK
#

use lib qw(@perlpath@);

use strict;

use File::Basename;
use File::Path;
use CGI qw(:cgi :oldstyle_urls);
use POSIX qw(:errno_h);

use Inf;
use AdminUtil;
use DSUtil;
use Resource;
use DSCreate qw(removeDSInstance);

print "Content-type: text/plain\n\n";

my $res = new Resource("@propertydir@/ds_remove.res",
                       "@propertydir@/setup-ds-admin.res",
                       "@propertydir@/setup-ds.res");

# parse the input parameters
my $query = new CGI;
my @errs;
my $force = $query->param('force');
if (!defined($force) || (length($force) == 0)) {
    $force = 1; # force use of force for CGI
}
my $instname = $query->param('InstanceName');
my ($slapd, $inst) = split(/-/, $instname, 2);
my $baseconfigdir = $ENV{DS_CONFIG_DIR} || "@instconfigdir@";
my $configdir = "$baseconfigdir/slapd-$inst";
my $status = 0;
if ( ! -d $configdir )
{
    print "NMC_ErrInfo: could not read $configdir - Error: $!\n";
    print STDERR "Error: could not read $configdir - Error: $!\n";
    # look for error other than "not found"
    if ($! != ENOENT) { # not found is ok
        $status = 1;
    }
    if (!$force) {
        exit 1;
    }
}

# NOTE about @errs - the return value will be an array
# or an array of array refs - usually the last element
# of the array will be the errno
# first, gather the information needed by unregister
my $inf = createInfFromConfig($configdir, $inst, \@errs);
if (@errs)
{
    print "NMC_ErrInfo: ", $res->getText(@errs), "\n";
    print STDERR "Error: ", $res->getText(@errs), "\n";
    # look for error other than "not found"
    if ($errs[-1] != ENOENT) { # not found is ok
        $status = 1;
    } 
    if (!$force) {
        exit 1;
    }
}
if (!$inf) {
    $inf = new Inf; # create empty one
}

# next, remove the instance
@errs = removeDSInstance($inst, $force);
if (@errs) {
    my $realerror;
    for (@errs) {
        my $text = $res->getText($_);
        print "NMC_ErrInfo: $text\n";
        print STDERR "Error: $text\n";
        if ($_->[-1] != ENOENT) { # not found is ok
            $realerror = 1;
        }
    }
    if ($realerror) {
        $status = 1;
    }
    if (!$force) {
        exit 1;
    }
}

# add the parmeters necessary to configure this DS to be managed
# by the console and to be registered with the config DS - these
# are usually passed in via the CGI params, or use reasonable
# default values
my $admConf = getAdmConf("$baseconfigdir/admin-serv");
$inf->{General}->{ConfigDirectoryLdapURL} = $query->param('ldap_url') ||
    $admConf->{ldapurl};
$inf->{General}->{AdminDomain} = $query->param('admin_domain') ||
    $admConf->{AdminDomain};
my $admpw = AdminUtil::getAdmpw($admConf);
if ($admpw && %{$admpw}) {
    unless (defined($inf->{admin}->{ServerAdminID})) {
        $inf->{admin}->{ServerAdminID} = $admpw->{ServerAdminID};
    }
}

# Unregister the server from the configuration ds
# get config ds url from input or admconf
# get admin id from input or admconf
# get admin domain
# config ds info
if (!unregisterDSWithConfigDS($inst, \@errs, $inf) && !$force)
{
    print "NMC_ErrInfo: ", $res->getText(@errs), "\n";
    $status = 1;
    print STDERR "Error:", $res->getText(@errs), "\n";
    if (!$force) {
        exit 1;
    }
}

if ( 1 == isConfigDS($instname, "$baseconfigdir/admin-serv") )
{
    # if it is the Config DS, adm.conf and local.conf needs to be removed.
    unlink("$baseconfigdir/admin-serv/adm.conf");
    unlink("$baseconfigdir/admin-serv/local.conf");
}

exit 0;

END {
    # report status, no matter where or when exit was called
    print "NMC_Status: $status\n";
}
