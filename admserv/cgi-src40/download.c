/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*///////////////////////////////////////////////////////*/
/*/                                                     /*/
/*/ David Tompkins (mailto:dt@netscape.com)             /*/
/*/ Netscape Communications Corp.                       /*/
/*/ 3/4/97                                              /*/
/*/                                                     /*/
/*/ download: returns the available nmcc downloads      /*/
/*/                                                     /*/
/*/ 4/13/98: Changed to return the entire page...       /*/
/*/ 4/27/98: Now reads the page from start-console.html /*/
/*/          and inserts the menu options.              /*/
/*/                                                     /*/
/*///////////////////////////////////////////////////////*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#ifdef XP_UNIX
#   include <dirent.h>
#   define SYS_DIR DIR
#   define SYS_DIRENT struct dirent
#   define dir_open opendir
#   define dir_read readdir
#   define dir_close closedir
#   define dir_name(de) (de->d_name)
#   define FILE_SEP '/'
#   define STRNCASECMP strncasecmp
#   define STRCASECMP  strcasecmp
#   define STRNCMP     strncmp
#   define STRDUP      strdup
#else /* XP_WIN32 */
#   include <prio.h>
#   define SYS_DIR PRDir
#   define SYS_DIRENT PRDirEntry
#   define dir_open PR_OpenDir
#   define dir_read(d) PR_ReadDir(d, PR_SKIP_BOTH)
#   define dir_close PR_CloseDir
#   define dir_name(de) (de->name)
#   define FILE_SEP '/'
#   define STRNCASECMP _strnicmp
#   define STRCASECMP  _stricmp
#   define STRNCMP     strncmp
#   define STRDUP      strdup
#endif

#include "config.h"

#define PATH_LENGTH		1024

#define MYHTMLFILE      "admserv.html"

static int
safe_snprintf(char *buf, size_t size, const char *fmt, ...)
{
    int ret;
    va_list ap;
    va_start(ap, fmt);
    ret = vsnprintf(buf, size, fmt, ap);
    va_end(ap);
    buf[size-1] = 0;
    return ret;
}

int
error_exit(char *msg)
{
   printf("Content-type: text/plain\n\n");
   printf("Error: %s\n", msg);
   exit(0);
}

int
main(int argc, char *argv[])
{
   char  line[PATH_LENGTH];
   FILE *html;
   char          *acceptLanguage = NULL;
   char          *loc;

   acceptLanguage = getenv("HTTP_ACCEPT_LANGUAGE");
   if (acceptLanguage == NULL)
   {
      acceptLanguage = "en";
   }

   /*
    * Search for list of accept language for a match
    */
   loc = strtok(strdup(acceptLanguage), ",");
   while (1)
   {
      safe_snprintf(line, sizeof(line), "%s%c%c%c%c%s",
                    HTMLDIR, FILE_SEP, loc[0],loc[1], FILE_SEP, MYHTMLFILE);
      if ((html = fopen(line, "r")) != NULL)
      {
         loc[2] = '\0';
         break;
      }
      else
      {
         loc = strtok(NULL, ", ");
         if (loc == NULL)
         {
            safe_snprintf(line, sizeof(line), "%s%c%s",
                          HTMLDIR, FILE_SEP, MYHTMLFILE);
            if ((html = fopen(line, "r")) == NULL)
            {
              error_exit(MYHTMLFILE " not found");
            }
            loc = "";
            break;
         }
      }
   } 

   printf("Content-type: text/html\n\n");

   while (fgets(line, PATH_LENGTH, html))
   {
#define INCLUDEIFEXISTS "<!-- INCLUDEIFEXISTS "
      if (!strncmp(line, INCLUDEIFEXISTS, strlen(INCLUDEIFEXISTS))) {
         char *p = line + strlen(INCLUDEIFEXISTS);
         char *end = strstr(line, " -->");
         if (p && *p && end) {
            char includefile[PATH_LENGTH];
            FILE *includedhtml = NULL;
            *end = '\0';
            safe_snprintf(includefile, sizeof(includefile),
                          "%s%c%s%s%s",
                          HTMLDIR, FILE_SEP, loc ? loc : "",
                          loc ? "/" : "", p);
            includedhtml = fopen(includefile, "r");
            while (includedhtml && fgets(line, PATH_LENGTH, includedhtml)) {
               printf("%s", line);
            }
            if (includedhtml) {
               fclose(includedhtml);
               includedhtml = NULL;
            }
         }
         continue;
      }
      printf("%s", line);
   }
   
   fflush(stdout);
   fclose(html);

   return 0;
}
