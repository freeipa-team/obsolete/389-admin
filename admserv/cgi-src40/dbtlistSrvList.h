/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
 
#define CGI_NAME "listOldSrvs"
 
static char dbtlistOldSrvsid[] = "$DBT: listOldSrvs referenced v1 $";
 
#include "libadminutil/resource.h"

BEGIN_STR(listOldSrvs)
  /* external string used in listOldSrvs.c */
  ResDef( DBT_CGIID_, -1, dbtlistOldSrvsid )
  ResDef( DBT_NO_METHOD, 1, "No method is specified" )
  ResDef( DBT_UNKNOWN_METHOD, 2, "Unknown Method (%s)" )
  ResDef( DBT_NO_QUERY_STRING, 3, "No data in QUERY_STRING environment variable" )
  ResDef( DBT_ERROR_OPEN_FILE, 4, "Error open file: %s" )
  ResDef( DBT_ERROR_READ_FILE, 5, "Error readiong file: %s" )
  ResDef( DBT_ERROR_OPEN_DIR, 6, "Error open directory: %s" )
  ResDef( DBT_DBT_NO_OLD_SERVER_ROOT, 7, "No old server root specified" )
  ResDef( DBT_NO_SERVER_FOUND, 8, "No server found" )
END_STR(listOldSrvs)
