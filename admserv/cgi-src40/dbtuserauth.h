/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
 
#define CGI_NAME "userauth"
 
static char dbtuserauthid[] = "$DBT: userAuth referenced v1 $";
 
#include "libadminutil/resource.h"

BEGIN_STR(userauth)
  /* external string used in migrateConfig.c */
  ResDef( DBT_CGIID_, -1, dbtuserauthid )
  ResDef( DBT_NO_USER_NAME, 1, "No user name is specified" )
  ResDef( DBT_NO_LDAP_INFO, 2, "No LDAP information is available" )
  ResDef( DBT_NO_SR_INFO, 3, "No Server Root information is available" )
END_STR(userauth)
