/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * Description (setport.c)
 *
 *	This module contains routines used by administration CGI
 *	programs to set the port number of Admin server
 *
 *      This is just a prototype
 */
#include <stdio.h>
#include <string.h>
#include "libadminutil/distadm.h"
#include "libadminutil/admutil.h"
#include "libadmsslutil/psetcssl.h"
#include "libadmsslutil/admsslutil.h"

int main(int argc, char *argv[])
{
  int            _ai = ADMUTIL_Init();
  PsetHndl       pset;
  char           *method;
  int            rv;
  char           *url;
  int            err;
  int            *errp = &err;
  char           *username = 0;
  char           *localAdmin = 0;
  char           *binddn = 0;
  char           *bindpw = 0;
  char           *newValue = 0;
  char           *portVal = 0;
  char           error_info[128];
  const char *configdir = util_get_conf_dir();
  const char *secdir = util_get_security_dir();

  memset((void *)errp, 0, sizeof(int));
  method = getenv("REQUEST_METHOD");

  /* Get new port number */
  if (!method || !*method) {
    rpt_err(SYSTEM_ERROR, "No method is specified", NULL, NULL);
  }
  else if (!strcmp(method, "GET")) {
    /* UI CGI - either upgrade or normal admin CGI */
    char *qs = getenv("QUERY_STRING");
	
    if (!qs || !*qs) {
      rpt_err(INCORRECT_USAGE, "NO QUERY_STRING DATA", NULL, NULL);
    }
    else {
      get_begin(qs);
      newValue = get_cgi_var("value", 0, 0);
    }
  }
  else if (!strcmp(method, "POST")) {
    post_begin(stdin);
    newValue = get_cgi_var("value", 0, 0);
  }
  else {
    PR_snprintf(error_info, sizeof(error_info), "Unknown Method (%s)!\n", method);
    rpt_err(SYSTEM_ERROR, error_info, NULL, NULL);
  }

  if (!newValue) {
    rpt_err(INCORRECT_USAGE, "NO DATA", NULL, NULL);
  }
    

  /* Get UserDN and User Password  */

  rv = ADM_GetUserDNString(&err, &binddn);
  if (rv < 0 || !binddn || !*binddn) {
    rv = ADM_GetCurrentUsername(&err, &username);
    if (rv < 0 || !username || !*username) {
      rpt_err(ELEM_MISSING, "NO USER NAME", NULL, NULL);
    }
    else {
      /* No DN, maybe it is local super */
      /* No DN, maybe it is local super */
      localAdmin = admGetLocalAdmin(configdir, &rv);
      if (localAdmin) {
	if (strcmp(username, localAdmin)) {
	  rpt_err(ELEM_MISSING, "ErrorString: NO USER DN!", NULL, NULL);
	}
	else {
	  binddn = NULL;
	  bindpw = NULL;
	}
      }
      else {
	rpt_err(ELEM_MISSING, "ErrorString: NO USER DN!", NULL, NULL);
      }
    }
  }

  if (binddn) rv = ADM_GetCurrentPassword(&err, &bindpw);

  rv = ADMSSL_InitSimple(configdir, secdir, 1);
  if (rv) {
    rpt_err(APP_ERROR, "SSL related initialization failed", NULL, NULL);
  }

  /* Initialize the pset  */

  pset = psetCreateSSL("admin-serv", 
		       /* configRoot */ configdir,
		       /* userDN */ binddn, 
		       /* passwd */ bindpw,
		       /* errorcode */ &rv);

  if (!pset) {
    PR_snprintf(error_info, sizeof(error_info), "PSETERROR: %d", rv);
    rpt_err(APP_ERROR, "PSET CREATE Failed", NULL, error_info);
  }

  /* Set port number to new number  */

  rv = psetSetSingleValueAttr(pset,"configuration.nsServerPort", newValue);

  /* Send response back  */
  if (rv != 0) {
    PR_snprintf(error_info, sizeof(error_info), "PSETERROR: %d", rv);
    rpt_err(APP_ERROR, "PSET SET Failed", NULL, error_info);
  }

  portVal = psetGetAttrSingleValue(pset,"configuration.nsServerPort", &rv);

  if (rv != 0) { PR_snprintf(error_info, sizeof(error_info), "PSETERROR: %d", rv);
  rpt_err(APP_ERROR, "SET is OK, but Verify GET Error", NULL, error_info); }
else { rpt_success(NULL); fprintf(stdout, "Value: %s\n", portVal); }

}
