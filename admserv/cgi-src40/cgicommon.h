/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#ifndef CGICOMMON_H
#define CGICOMMON_H

#define COMMON_RESOURCE_FILE "cgicommon"

#define resource_key(a,b)   a b

/* These are the defines to use in C code for localized messages */
#define CMN_CONFIGPROBLEM      resource_key(COMMON_RESOURCE_FILE, "1")
#define CMN_SSL_INIT_ERROR     resource_key(COMMON_RESOURCE_FILE, "2")
#define CMN_NSS_SHUTDOWN_ERROR resource_key(COMMON_RESOURCE_FILE, "3")

/* These are other properties */
#define DSOBJECTCLASS "nsdirectoryserver" /* name of table in cgicommon */
#define DSCONFIGENTRY "cn=config"
#define DSSECURITYDIR "nsslapd-certdir"
#define DSERRORLOGDIR "nsslapd-errorlog"

#endif /* CGICOMMON_H */
