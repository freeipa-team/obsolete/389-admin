/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
 
#define CGI_NAME "restartsrv"
 
static char dbtrestartsrvid[] = "$DBT: restartsrv referenced v1 $";
 
#include "libadminutil/resource.h"

BEGIN_STR(restartsrv)
  /* external string used in migrateConfig.c */
  ResDef( DBT_CGIID_, -1, dbtrestartsrvid )
  ResDef( DBT_NO_NEW_PROCESS, 1, "Couldn't create a new process to stop admin server" )
  ResDef( DBT_RESTART_ACK, 2, "Admin server should restart on user request" )
  ResDef( DBT_CANT_STOP_SRV, 3, "Couldn't stop the administration server" )
END_STR(restartsrv)
