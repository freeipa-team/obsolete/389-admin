#!/bin/sh

if [ -z "$1" -o -d "$1" ] ; then
    echo "Usage: $0 output.tar.bz2 file"
    exit 1
fi

if [ -f "$1" ] ; then
    echo "File $1 exists - remove it first"
    exit 1
fi

if [ -n "$2" ] ; then
    dbfiles="var/lib/dirsrv/slapd-*/db/* var/lib/dirsrv/slapd-*/db/*/* var/lib/dirsrv/slapd-*/cldb/*"
fi

libdir=usr/lib64/dirsrv
if [ ! -d /$libdir ] ; then
    libdir=usr/lib/dirsrv
fi

# use new httpd.conf and admserv.conf
cd /
tar cpjf $1 -C / --no-recursion --exclude httpd.conf --exclude admserv.conf \
    etc/sysconfig/dirsrv* etc/dirsrv/slapd-* etc/dirsrv/slapd-*/* \
    etc/dirsrv/slapd-*/schema/* var/run/dirsrv var/lock/dirsrv/slapd-* \
    var/log/dirsrv/slapd-* var/lib/dirsrv/slapd-* var/lib/dirsrv/slapd-*/* \
    var/lib/dirsrv/slapd-*/ldif/*.ldif $libdir/slapd-* $libdir/slapd-*/start-slapd \
    etc/dirsrv/admin-serv etc/dirsrv/admin-serv/* var/log/dirsrv/admin-serv `echo $dbfiles`
ls -l $1
