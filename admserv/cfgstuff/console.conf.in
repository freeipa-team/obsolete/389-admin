# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK

<IfModule !mpm_winnt.c>
<IfModule !mpm_netware.c>
#
# If you wish httpd to run as a different user or group, you must run
# httpd as root initially and it will switch.  
#
# User/Group: The name (or #number) of the user/group to run httpd as.
#  . On SCO (ODT 3) use "User nouser" and "Group nogroup".
#  . On HPUX you may not be able to use shared memory as nobody, and the
#    suggested workaround is to create a user www and use that user.
#  NOTE that some kernels refuse to setgid(Group) or semctl(IPC_SET)
#  when the value of (unsigned)Group is above 60000; 
#  don't use Group #-1 on these systems!
#
User @httpduser@
Group @httpdgroup@
</IfModule>
</IfModule>

#
# PidFile: The file in which the server should record its process
# identification number when it starts.
#
<IfModule !mpm_netware.c>
PidFile @piddir@/@pidfile@
</IfModule>

#
# By default, the log files will only log the client IP address,
# not the hostname, to avoid having to do a DNS lookup
# for each request.  If HostnameLookups is off, you will also see
# notices in the error log saying that
# admserv_host_ip_check: ap_get_remote_host could not resolve the IP address
# If you want to have hostnames in the log instead of IP addresses, change
# this to "on".  Use a value of "double" to make it do double reverse DNS lookups.
HostnameLookups off

#
# The location and format of the access logfile (Common Logfile Format).
# If you do not define any access logfiles within a <VirtualHost>
# container, they will be logged here.  Contrariwise, if you *do*
# define per-<VirtualHost> access logfiles, transactions will be
# logged therein and *not* in this file.
#
CustomLog @logdir@/access common

#
# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.  If you *do* define an error logfile for a <VirtualHost>
# container, that host's errors will be logged there and not here.
#
ErrorLog @logdir@/error

#
# Listen: Allows you to bind Apache to specific IP addresses and/or
# ports, in addition to the default. See also the <VirtualHost>
# directive.
#
# Change this to Listen on specific IP addresses as shown below to 
# prevent Apache from glomming onto all bound IP addresses (0.0.0.0)
# e.g. "Listen 12.34.56.78:9830"
#
# To allow connections to use IPv6 addresses add "Listen [::]:9830"
#
Listen @admservip@:@admservport@

#   SSL Engine Switch:
#   Enable/Disable SSL for this virtual host.
NSSEngine off

#   SSL Certificate Nickname:
#   The nickname of the server certificate you are going to use.
NSSNickname server-cert

#   Server Certificate Database:
#   The NSS security database directory that holds the certificates and
#   keys. The database consists of 3 files: cert8.db, key3.db and secmod.db.
#   Provide the directory that these files exist.
NSSCertificateDatabase @securitydir@
# no prefix anymore - assumes securitydir has only the sec files for admin server
#NSSDBPrefix %%%instancename%%%-

#   SSL Cipher Suite:
#   List the ciphers that the client is permitted to negotiate.
#   See the mod_nss documentation for a complete list.
# SSL 3 ciphers. SSL 2 is disabled by default.
NSSCipherSuite +rsa_rc4_128_md5,+rsa_rc4_128_sha,+rsa_3des_sha,-rsa_des_sha,-rsa_rc4_40_md5,-rsa_rc2_40_md5,-rsa_null_md5,-rsa_null_sha,+fips_3des_sha,-fips_des_sha,-fortezza,-fortezza_rc4_128_sha,-fortezza_null,-rsa_des_56_sha,-rsa_rc4_56_sha,+rsa_aes_128_sha,+rsa_aes_256_sha

NSSProtocol TLSv1.1

#   Client Authentication (Type):
#   Client certificate verification type.  Types are none, optional and
#   require.
NSSVerifyClient none
