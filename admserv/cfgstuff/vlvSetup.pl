# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

# END COPYRIGHT BLOCK
#!../install/perl -w
#
# Script to setup default directory vlv for admin server
# 

die "Usage: vlvSetup.pl <Directory Manager's DN> <Directory Manager's password> <base object> <host> [port #]\n" unless (($#ARGV >= 3) && ($#ARGV <= 4));

if ((!((-e "vlvindex.bat") || (-e "vlvindex"))) ||
    (!((-e "restart-slapd.bat") || (-e "restart-slapd")))) {
    print "THIS SCRIPT MUST BE RUN FROM DIRECTORY INSTANCE ROOT\n";
    exit;
}


$dirmgr = $ARGV[0];  # directory manager dn
$passwd = $ARGV[1];  # directory manager password
$baseobject = $ARGV[2];  # directory server host
$host   = $ARGV[3];  # directory server host
$port   = 389;       # default directory port
$port   = $ARGV[4] if ($ARGV[4]); # directory port


$ldapmodify = "../shared/bin/ldapmodify";

# add default vlv index for user and group
open (LDAP, "| $ldapmodify -h $host -p $port -F -c -v -a -D \"$dirmgr\" -w $passwd");
print LDAP "dn: cn=defaultConsoleVLV,cn=config,cn=ldbm\n";
print LDAP "objectclass: top\n";
print LDAP "objectclass: vlvSearch\n";
print LDAP "cn: defaultConsoleVLV\n";
print LDAP "vlvBase: $baseobject\n";
print LDAP "vlvScope: 2\n";
print LDAP "vlvFilter: (|(objectclass=person)(objectclass=groupofuniquenames)(objectclass=organizationalunit))\n";
print LDAP "aci: (target=\"ldap:///cn=defaultConsoleVLV, cn=config, cn=ldbm\")(targetattr = \"*\") (version 3.0; acl \"Config\"; allow( read, search, compare ) userdn = \"ldap:///anyone\";)\n";
print LDAP "\n\n";
print LDAP "dn: cn=defaultConsoleVLVForward_CN,cn=defaultConsoleVLV,cn=config,cn=ldbm\n";
print LDAP "changetype: add\n";
print LDAP "objectclass: top\n";
print LDAP "objectclass: vlvIndex\n";
print LDAP "cn: defaultConsoleVLVForward_CN\n";
print LDAP "vlvSort: cn\n";
print LDAP "\n";
close (LDAP);


@args = ("vlvindex", "defaultConsoleVLVForward_CN");
system(@args) == 0 or die "create a vlvindex failed\n";
print "\n";

print "Restart directory server (y/n): ";

$choice = <STDIN>; 
chomp($choice);
if ($choice eq 'y') { 
    @args = ("restart-slapd");
    system(@args) == 0 or die "directory restart failed\n";
    print "\n";
}
