# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 2
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# END COPYRIGHT BLOCK
#
# To get the Admin Express working properly, add the following to 
# apache, either directly or thru an Include /path/to/this.conf

ADMCacheLifeTime 600
ADMServerVersionString "@capbrand@-Administrator/@PACKAGE_VERSION@"
ADMConfigDir "@configdir@"

ScriptAlias /dist/ "@cgibindir@/"
ScriptAlias /manual/help/ "@cgibindir@/"

SetEnv LD_LIBRARY_PATH @LIBPATH@
# this is primarily needed for HP-UX and the other platforms
# where we bundle all of the components together
# so that adminutil can find its property files
# In FHS or explicit prefix installs, this is not needed
# nor used
SetEnv ADMINUTIL_CONF_DIR @adminutilpath@

# all access is explicitly denied by default in httpd.conf
# the following Directory directives turn on access for specific
# directories
<Directory "@htmldir@/">
    AllowOverride None
    Options FollowSymLinks
    Order allow,deny
    Allow from all
</Directory>

<Directory "@manualdir@/">
    AllowOverride None
    Options None
    Order allow,deny
    Allow from all
</Directory>

<Directory "@icondir@/">
    AllowOverride None
    Options None
    Order allow,deny
    Allow from all
</Directory>

# remap / requests to the download CGI
RewriteEngine on
RewriteRule ^/$ /dist/download [R,L,QSA]

# remap admin server icons
Alias /admin-serv/tasks/icons/ @icondir@/
Alias /icons/ @icondir@/

# Handle the initial login
<Location /admin-serv/authenticate>
    SetHandler user-auth
    AuthUserFile @configdir@/admpw
    AuthType basic
    AuthName "Admin Server"
    Require valid-user
    Order allow,deny
    Allow from all
</Location>

# Handle Other Console tasks
<LocationMatch /*/[tT]asks/[Oo]peration/*>
  AuthUserFile @configdir@/admpw
  AuthType basic
  AuthName "Admin Server"
  Require valid-user
  AdminSDK on
  ADMCgiBinDir @cgibindir@
  Options +ExecCGI
  Order allow,deny
  Allow from all
</LocationMatch>

# Handle Admin Express
<LocationMatch /*/[tT]asks/[Cc]onfiguration/*>
  AuthUserFile @configdir@/admpw
  AuthType basic
  AuthName "Admin Server"
  Require valid-user
  AdminSDK on
  ADMCgiBinDir @cgibindir@
  NESCompatEnv on
  Options +ExecCGI
  Order allow,deny
  Allow from all
</LocationMatch>

# Handle internal commands
<LocationMatch /admin-serv/commands/*>
  AuthUserFile @configdir@/admpw
  AuthType basic
  AuthName "Admin Server"
  Require valid-user
  Order allow,deny
  Allow from all
</LocationMatch>

# Handle Stop, Start, Restart, Instance Creation - invoke mod_restartd
# need to add instance creation because you may want to create an instance
# of DS on a low port, and instance creation starts the instance as well
<LocationMatch /*/[tT]asks/[Oo]peration/(?i:stop|start|restart|startconfigds|create|remove)$>
  AuthUserFile @configdir@/admpw
  AuthType basic
  AuthName "Admin Server"
  Require valid-user
## turn off the password pipe when using mod_restartd
  AdminSDK off
  ADMCgiBinDir @cgibindir@
  Options +ExecCGI
  RetainPerms on
  Order allow,deny
  Allow from all
</LocationMatch>
