/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* 
 * List the database backup directories.
 * No HTML - this is for DS 4.0.
 *
 * Rob Weltman
 */

#include <stdio.h>
#include <stdlib.h>
#include "dsalib.h"

int main(int argc, char *argv[], char *envp[])
{
    char **bak_dirs;
    char **ds_config = NULL;
    int i = 0;

    ds_config = ds_get_config (DS_REAL_CONFIG);
    ds_become_localuser(ds_config);
    ds_free_config(ds_config);

    /* Tell the receiver we are about to start sending data */
    fprintf(stdout, "\n");
    bak_dirs = ds_get_bak_dirs();
    if (bak_dirs != NULL )	/* no error */ {
        for (i = 0; bak_dirs[i]; i++) {
            fprintf(stdout, "%s\n", bak_dirs[i]);
        }
    }

    ds_become_original();

    return 0;
}
