/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*
 * Set up environment for CGIs.
 * 
 * Rob Weltman
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libadminutil/admutil.h"
#include "libadminutil/distadm.h"
#include "init_ds_env.h"
#include "dsalib.h"
#include "prprf.h"

int init_ds_env()
{
    char *m = getenv("REQUEST_METHOD");
    char *qs              = NULL;
	int proceed = 0;

	(void)ADMUTIL_Init();
	if ( m != NULL ) {
		if( !strcmp(m, "GET") )  {
			qs = GET_QUERY_STRING();
			if ( qs && *qs ) {
				get_begin(qs);
			}
			proceed = 1;
		} else if(!strcmp(m, "POST"))  {
			if (post_begin(stdin)) {
				proceed = 0;
			} else {
				proceed = 1;
			}
		}
	}
 
	if(!proceed) {
		char msg[2000];
		PR_snprintf(msg, sizeof(msg), "ErrorString: REQUEST_METHOD=%s,"
				"QUERY_STRING=%s\n", 
				(m == NULL) ? "<undefined>" : m, 
				(qs == NULL) ? "<undefined>" : qs);
		rpt_err( GENERAL_FAILURE,
				 msg,
				 "",
				 "" );
		return 1;
	}

	return 0;
}
