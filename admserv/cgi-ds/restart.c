/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*
 * restart.c:  Stops and the starts up the server.
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libadminutil/admutil.h"
#include "dsalib.h"
#include "init_ds_env.h"

#ifdef XP_WIN32
  #define sleep(sec) Sleep(sec)
#endif

int main(int argc, char *argv[])
{
    int status = -1;

    fprintf(stdout, "Content-type: text/html\n\n");

	if ( init_ds_env() )
		return 1;

	if (DS_SERVER_UP == ds_get_updown_status()) {
		status = ds_bring_down_server();
		if(status != DS_SERVER_DOWN)  {
			rpt_err( status, "", NULL,  NULL );
			return 1;
		}
	}
	status = ds_bring_up_server(1);
	if(status == DS_SERVER_UP)  {
		rpt_success("Success! The server has been restarted.");
		return 0;
	}  else  {
		rpt_err( status, "", NULL,  NULL );
		return 1;
	}
}
