/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * END COPYRIGHT BLOCK **/
/************************************************************/
/*                                                          */
/* mod_admserv.h : defines for the admin server module      */
/*                                                          */
/* Adam Prishtina 1/13/00                                   */
/*                                                          */
/************************************************************/

#ifndef _ADMSERV_PLUGIN_H
#define _ADMSERV_PLUGIN_H

#include <ldap.h>

#include "libadmin/libadmin.h"

#include "apr_hash.h"

#define LDAPU_SUCCESS 0
#define LDAPU_FAILED                      -1
#define LDAPU_ERR_OUT_OF_MEMORY           -110
#define LDAPU_ERR_MULTIPLE_MATCHES        -194

#ifdef XP_UNIX
#   define STRNCASECMP strncasecmp
#   define STRCASECMP  strcasecmp
#   define STRCMP      strcmp
#else /* XP_WIN32 */
#   define STRNCASECMP _strnicmp
#   define STRCASECMP  _stricmp
#   define STRCMP      _stricmp
#endif

#ifndef FILE_PATHSEP
#define FILE_PATHSEP '/'
#endif

#ifndef PATH_MAX
#define PATH_MAX 256
#endif

#ifndef LINE_LENGTH
#define LINE_LENGTH 1024
#endif

#ifndef ACCESS_DENIED
#define ACCESS_DENIED 0
#endif

#ifndef ACCESS_GRANTED
#define ACCESS_GRANTED 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define ADMIN_HASH_SIZE 		2
#define LDAP_BASE_PREFIX 		(char*)"ldap://"
#define LDAPS_BASE_PREFIX 		(char*)"ldaps://"
#define LDAP_BASE_PREFIX_LENGTH		(strlen(LDAP_BASE_PREFIX))
#define LDAPS_BASE_PREFIX_LENGTH	(strlen(LDAPS_BASE_PREFIX))
#define LDAP_PREFIX        		(char*)"ldap:///"
#define LDAP_PREFIX_LENGTH 		(strlen(LDAP_PREFIX))
#define ADMIN_SERVER_ID 		(char*)"admin-serv"
#define TASK_IDENTIFIER 		(char*)"tasks/"
#define LOCAL_SUPER_NAME 		(char*)"LocalSuper"
#define AUTHENTICATION_LDAP_URL 	(char*)"AuthenticationLdapURL"
#define RUNTIME_RESYNC_COMMAND 		(char*)"sync-task-sie-data"
#define CHANGE_SIEPWD_COMMAND		(char*)"change-sie-password"

#define RUNTIME_COMMAND_CONTENT_TYPE 	(char*)"admin-internal/command"
#define DOWNLOAD_CONTENT_TYPE		(char*)"application/octet-stream"
#define SERVLET_CONTENT_TYPE		(char*)"magnus-internal/servlet"

typedef struct {
    apr_hash_t *table;
} HashTable;
 
typedef struct RebindData {
   char *user;
   char *pw;
} RebindData;

typedef struct UserCacheEntry {
   char *userDN;
   char *userPW;
   char *ldapURL;
   long  createTime;
} UserCacheEntry;

typedef struct TaskCacheEntry {
   char      *execRef;
   char      *execRefArgs;
   int        logSuppress;
   HashTable *auth_userDNs;
} TaskCacheEntry;

typedef struct PopulateTasksData {
   LDAP *server;
   char *userDN;
   long  now;
} PopulateTasksData;

typedef struct LdapServerData {
   DWORD dPad;
   char *host;
   int   port;
   int 	 secure;  /* track whether the server is running in secure mode */
   char *baseDN;  /* for the config ds, usually o=NetscapeRoot
                     for the user/group ds, this is the default
                     suffix e.g. dc=example,dc=com */
   char *bindDN;  /* deprecated since the SIE cannot bind anymore */
   char *bindPW;  /* deprecated since the SIE cannot bind anymore */
   char *admservSieDN; /* SIE DN of this admin server */
   char *securitydir; /* path to key/cert databases */
} LdapServerData;

typedef struct ServletLookupData {
   char *nsExecRef;
   char *taskcn;
   char *serverDN;
   char *serverID;
} ServletLookupData;

typedef int (*RuntimeCommandFn)(const char *, char *, void *, request_rec*);

typedef struct RuntimeCommandRecord {
   RuntimeCommandFn fn;
   void            *arg;
} RuntimeCommandRecord;

#ifndef LDAP_CONTROL_PWEXPIRED
#define LDAP_CONTROL_PWEXPIRED          "2.16.840.1.113730.3.4.4"
#endif
#ifndef LDAP_CONTROL_PWEXPIRING
#define LDAP_CONTROL_PWEXPIRING         "2.16.840.1.113730.3.4.5"
#endif

#endif
