/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * error.c - Handle error recovery
 *
 * All blame to Mike McCool
 */

#include "libadmin/libadmin.h"
#if 0
#include "cgiutils/cgi-util.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef XP_WIN32
#include <windows.h>
#include "base/nterr.h"
#endif

#define ERROR_HTML "error.html"

/* Be sure to edit libadmin.h and add new #define types for these headers. */
char *error_headers[MAX_ERROR] =
  {"File System Error",
   "Memory Error",
   "System Error",
   "Incorrect Usage",
   "Form Element Missing",
   "Registry Database Error",
   "Network Error",
   "Unexpected Failure",
   "Warning"};

#ifdef XP_UNIX
#define get_error() errno
#define verbose_error() system_errmsg()
#else /* XP_WIN32 */
int get_error()
{
    int error = GetLastError();
    return(error ? error: WSAGetLastError());
}
char *verbose_error()
{
    /* Initialize error hash tables */
    HashNtErrors();
    return alert_word_wrap(system_errmsg(), WORD_WRAP_WIDTH, "\\n");
}
#endif /* XP_WIN32 */

void _report_error(int type, char *info, char *details, int shouldexit)
{
    /* Be sure headers are terminated. */
    fputs("\n", stdout);

    fprintf(stdout, "<SCRIPT LANGUAGE=\"%s\">", MOCHA_NAME);
    output_alert(type, info, details, 0);
    if(shouldexit)  {
        fprintf(stdout, "if(history.length>1) history.back();"); 
    }
    fprintf(stdout, "</SCRIPT>\n");

    if(shouldexit)  {
        WSACleanup();
        exit(0);
    }
}

/*
 * Format and output a call to the JavaScript alert() function.
 * The caller must ensure a JavaScript context.
 */
NSAPI_PUBLIC void output_alert(int type, char *info, char *details, int wait)
{
    char *wrapped=NULL;
    int err;

    if(type >= MAX_ERROR)
        type=DEFAULT_ERROR;

    wrapped=alert_word_wrap(details, WORD_WRAP_WIDTH, "\\n");

    if(!info) info="";
    fprintf(stdout, (wait) ? "confirm(\"" : "alert(\"");
    fprintf(stdout, "%s:%s\\n%s", error_headers[type], info, wrapped);
    if(type==FILE_ERROR || type==SYSTEM_ERROR)  {
        err = get_error();
        if(err != 0)
            fprintf(stdout,
                        "\\n\\nThe system returned error number %d, "
                        "which is %s.", err, verbose_error());
    }
    fprintf(stdout, "\");");
}

NSAPI_PUBLIC void report_error(int type, char *info, char *details)
{
    _report_error(type, info, details, 1);
}

NSAPI_PUBLIC void report_warning(int type, char *info, char *details)
{
    _report_error(type, info, details, 0);
}

