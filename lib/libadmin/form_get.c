/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * form_get.c - Functions to help with handling GET requests
 *
 * All blame to Mike McCool
 */

#include "libadmin/libadmin.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <i18n.h>
#include "prprf.h"

NSAPI_PUBLIC FILE *open_error_file(char *filename)
{
    FILE *f = NULL;
    char line[BIG_LINE];

    PR_snprintf(line, sizeof(line), "%s%c%s", util_get_html_dir(), FILE_PATHSEP, filename);

    if(!(f = fopen(line, "r")))  {
        fprintf(stdout, "\n<h1 align=center>Catastrophic error</h1>\n");
        fprintf(stdout, "Cannot open any administrative HTML files (errno "
                        "is %d).\n", errno);
        exit(0);
    }
    return f;
}

NSAPI_PUBLIC FILE *open_html_file(char *filename)
{
    FILE *f = NULL;
    char line[BIG_LINE];

    PR_snprintf(line, sizeof(line), "%s%c%s", util_get_html_dir(), FILE_PATHSEP, filename);
    if(!(f = fopen(line, "r")))  {
        report_error(FILE_ERROR, line, "Could not open the HTML file.  "
                     "Perhaps the permissions have changed or someone "
                     "has moved the file.");
    }
    return f;
}

NSAPI_PUBLIC int next_html_line(FILE *f, char *line)
{
    if(!(fgets(line, BIG_LINE, f)))  {
#ifdef XP_UNIX
        fclose(f);
        return 0;
#else /* XP_WIN32 */
		if (feof(f)) {
        	fclose(f);
        	return 0;
		}
#endif /* XP_WIN32 */
    }
    else
        return 1;
}

NSAPI_PUBLIC void output_input(char *type, char *name, char *value, char *other)
{
    char * cp;
    char * dp;
    int qcnt = 0;

    /* Do a backslash quote on any double quotes in the value string */
    if (value) {

	/* Count the double-quotes */
	cp = value;
	while ((cp = strpbrk(cp, "\"")) != 0) {
	    ++cp;
	    ++qcnt;
	}

	if (qcnt > 0) {
	    /* Allocate buffer for result string */
	    dp = (char *)MALLOC(strlen(value) + (qcnt * 5) + 1);
	    cp = value;
	    value = dp;
	    while (*cp) {
		if (*cp == '"') {
		    strcpy(dp, "&quot;");
		    dp += 6;
		    cp += 1;
		}
		else {
		    *dp++ = *cp++;
		}
	    }
	    *dp = 0;
	}
    }
	
    fprintf(stdout, "<INPUT type=\"%s\" name=\"%s\" value=\"%s\" %s>%s",
                     type, name, (value) ? value : "", (other) ? other : "",
                     ((strcmp(type, "radio")) && (strcmp(type, "checkbox"))) ? 
                       "\n" : "");

    if (value && (qcnt > 0)) {
	FREE(value);
    }
}
