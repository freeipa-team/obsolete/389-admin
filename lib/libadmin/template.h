/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * template.h:  Header for HTML page template directives
 *
 * All blame to Mike McCool
 */

#ifndef __template__
#define __template__

/* The maximum number of variables for a given template */
#define MAXVARS 4

/* The structure of a directive is fairly simple.  You have:
 *
 * <!-- NAME var1="val" var2="val" var3="val">
 *
 * You _must_ put the values in quotes. 
 */

/* The structure of a template.  */
typedef struct template_s {
    char *name;
    char *format;
} *tmpptr;

#define DIRECTIVE_START "<!-- "
#define DIRECTIVE_END '>'

/* Document here the templates defined in template.c */
/* These will go into a master list: templates[MAXTEMPLATE] */
/* See template.c for a specification of how they should work */

/* Conditionals */
/* 1. IF - <!-- IF "var#">, where var# is the offset into the array of vars
 *                        sent to the template matcher.  Puts the text in
 *                        if true. */
/* 2. ELSE - <!-- ELSE>, ends an if and starts the text following if the
 *                       if was not true. */
/* 3. ENDIF - <!-- ENDIF>, ends the if statement and goes to normal. */

/* 4.  TITLE - no vars, just puts up the document title */
/* 5.  PAGEHEADER - puts up the header for a page.  Takes 3 variables:
 *                  1.  Left hand graphic, relative path
 *                  2.  Right hand graphic, relative path
 *                  3.  Text in center */
/* 6.  DOCSWITCHER - puts up a header with a document switcher and an
 *                   indication of the current track */
/* 7.  COPYRIGHT - no vars, just puts up the copyright notice */
/* 8.  RSRCPCKR - The resource picker to go at the top of each page.  
 */
/* 9.  BOOKTRACK - put up the little book that you can click on to choose
 *                 docs or no docs.
 *                  1.  The open book href
 *                  2.  The closed book href
 *                  3.  The desired image alignment
 */
/* 10. BEGININFO - the header to a documentation block
 *                  1.  The actual text to put in
 */
/* 11. ENDINFO - end the documentation block */
/* 12. SUBMIT - Put the submit buttons at the bottom of a form. No vars */
/* 13. DOCUMENTROOT - Print out the current document root.  
 *                  1.  Trailer - a directory (if any) to put after the
 *                      document root, for example $DOCUMENT_ROOT/admin/config
 */
/* 14. ELEMBEGIN - Begin the form elements */
/* 15. ELEM - Put the style around a form element
 *                  1. Text - a short description of it
 */
/* 16. ELEMEND - end a form element
 */
/* 17. ELEMADD - add another element without a HR 
 */
/* 18. ELEMDIV - put in an element divider.
 */
/* 19. REFERER - an anchor containing the referer, with optional text 
 *               for inside the anchor.  If no text is specified, it will
 *               be the script name. 
 *                  1. Text - optional
 */
/* 20. INDEX - put a link to the index.  Takes one var: what to put in the
 *             text of the anchor.
 */
/* 21. SERVERROOT - prints out the ServerRoot.  Takes one var: what directory
 *                  to put after the ServerRoot (i.e. admin/userdb)
 */
/* 22. RESTART - a pointer to the restart signal. */
/* 23. ACCESS - Access the server as a client. */
/* 24. COMMIT - a pointer to the script to commit your changes and restart
 *              the server. */
/* 25. ADDINFO - add more info to the bottom of an info block */
/* 26. BACKOUT - at the top, everyone has a link to return to the server
 *               manager.  
 *                   1. text for what you don't want to do
 */
/* 27. CURSERVNAME - the name of the first server in the list. 
 */
/* 28. VERIFY - a submit button set, except it has a verification 
 *              dialog box that comes up.
 *                   1. the text to bring up, as in 'Do you really want to %s?'
 */
/*
 * 29. HELPLINK - output a help link. No options.
 */
/* 30. HELPBUTTON - output a help button. No options.
 */
/* 31. DIALOGSUBMIT - Output a submit with "Done" and "Cancel" instead 
 *                    of "OK" and "Reset", where cancel closes the window.
 */

#endif
