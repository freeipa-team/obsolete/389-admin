/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/
/*
 * system.c: A grab bag of system-level abstractions
 * 
 * Many authors
 */

#include "netsite.h"
#include "base/nsassert.h"

#ifdef MAGNUS_VERSION_STRING
#define MY_VERSION_STRING MAGNUS_VERSION_STRING
#else
#define MY_VERSION_STRING ADM_PACKAGE_VERSION
#endif

#ifdef XP_WIN32
#include <windows.h>
static char *version = MY_VERSION_STRING;
#endif

#include "base/systems.h"	/* find out if we have malloc pools */

static int thread_malloc_key = -1;

#ifdef MCC_DEBUG
#define DEBUG_MALLOC
#endif

#ifdef DEBUG_MALLOC

/* The debug malloc routines provide several functions:
 *
 *  - detect allocated memory overflow/underflow
 *  - detect multiple frees
 *  - intentionally clobbers malloc'd buffers
 *  - intentionally clobbers freed buffers
 */
#define DEBUG_MAGIC 0x12345678
#define DEBUG_MARGIN 32
#define DEBUG_MARGIN_CHAR '*'
#define DEBUG_MALLOC_CHAR '.'
#define DEBUG_FREE_CHAR   'X'
#endif /* DEBUG_MALLOC */

/* On NT, the server version string is not statically encoded based
 * upon a product compile define but dynamically set by the server
 * exe at startup.
 */

NSAPI_PUBLIC char *system_version()
{
#ifdef XP_WIN32
    return version;
#else /* XP_UNIX */
    return MY_VERSION_STRING;
#endif /* XP_UNIX */
}

NSAPI_PUBLIC void system_version_set(char *server_version)
{
#ifdef XP_WIN32
    version = PERM_STRDUP(server_version);
#endif
}


NSAPI_PUBLIC void *system_malloc(int size)
{
    return malloc(size);
}


NSAPI_PUBLIC void *system_calloc(int size)
{
    void *ret;
    ret = malloc(size);
    if(ret)
        ZERO(ret, size);
    return ret;
}


NSAPI_PUBLIC void *system_realloc(void *ptr, int size)
{
    return realloc(ptr, size);
}


NSAPI_PUBLIC void system_free(void *ptr)
{
    NS_ASSERT(ptr);
    free(ptr);
}

NSAPI_PUBLIC char *system_strdup(const char *ptr)
{
    NS_ASSERT(ptr);
    return strdup(ptr);
}


NSAPI_PUBLIC void *system_malloc_perm(int size)
{
#ifndef DEBUG_MALLOC
    return malloc(size);
#else
    char *ptr = (char *)malloc(size + 2*DEBUG_MARGIN+2*sizeof(int));
    char *real_ptr;
    int *magic;
    int *length;
  
    magic = (int *)ptr;
    *magic = DEBUG_MAGIC;
    ptr += sizeof(int);
    length = (int *)ptr;
    *length = size;
    ptr += sizeof(int);
    memset(ptr, DEBUG_MARGIN_CHAR, DEBUG_MARGIN);
    ptr += DEBUG_MARGIN;
    memset(ptr, DEBUG_MALLOC_CHAR, size);
    real_ptr = ptr;
    ptr += size;
    memset(ptr, DEBUG_MARGIN_CHAR, DEBUG_MARGIN);

    return real_ptr;
#endif
}

NSAPI_PUBLIC void *system_calloc_perm(int size)
{
    void *ret = system_malloc_perm(size);
    if(ret)
        ZERO(ret, size);
    return ret;
}

NSAPI_PUBLIC void *system_realloc_perm(void *ptr, int size)
{
#ifndef DEBUG_MALLOC
    return realloc(ptr, size);
#else
    int *magic, *length;
    char *baseptr;
    char *cptr;

    cptr = (char *)ptr - DEBUG_MARGIN - 2 * sizeof(int);
    magic = (int *)cptr;
    if (*magic == DEBUG_MAGIC) {
        cptr += sizeof(int);
        length = (int *)cptr;
        if (*length < size) {
            char *newptr = (char *)system_malloc_perm(size);
            memcpy(newptr, ptr, *length);
            system_free_perm(ptr);

            return newptr;
        }else {
            return ptr;
        }
    } else {
        return realloc(ptr, size);
    }

#endif
}

NSAPI_PUBLIC void system_free_perm(void *ptr)
{
#ifdef DEBUG_MALLOC
    int *length, *magic;
    char *baseptr, *cptr;
    int index;

    NS_ASSERT(ptr);

    cptr = baseptr = ((char *)ptr) - DEBUG_MARGIN - 2*sizeof(int);

    magic = (int *)cptr;
    if (*magic == DEBUG_MAGIC) {
        cptr += sizeof(int);

        length = (int *)cptr;

        cptr += sizeof(int); 
        for (index=0; index<DEBUG_MARGIN; index++)
            if (cptr[index] != DEBUG_MARGIN_CHAR) {
                break;
            }

        cptr += DEBUG_MARGIN + *length;
        for (index=0; index<DEBUG_MARGIN; index++)
            if (cptr[index] != DEBUG_MARGIN_CHAR) {
                break;
            }

        memset(baseptr, DEBUG_FREE_CHAR, *length + 2*DEBUG_MARGIN+sizeof(int));
    }
    free(baseptr);
#else
    free(ptr);
#endif
}

NSAPI_PUBLIC char *system_strdup_perm(const char *ptr)
{
#ifndef DEBUG_MALLOC
    NS_ASSERT(ptr);
    return strdup(ptr);
#else
    int len = strlen(ptr);
    char *nptr = (char *)system_malloc_perm(len+1);
    memcpy(nptr, ptr, len);
    nptr[len] = '\0';
    return nptr;
#endif
}

NSAPI_PUBLIC int 
getThreadMallocKey(void)
{
    return thread_malloc_key;
}

void
setThreadMallocKey(int key)
{
    thread_malloc_key = key;
}

