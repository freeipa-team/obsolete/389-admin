/** BEGIN COPYRIGHT BLOCK
 * This Program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; version 2 of the License.
 * 
 * This Program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this Program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 * 
 * Copyright (C) 2001 Sun Microsystems, Inc. Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * END COPYRIGHT BLOCK **/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#if defined( XP_WIN32 )
#include <windows.h>
#include <process.h>
#include <io.h>
#endif
#include "dsalib.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#if !defined( XP_WIN32 )
#include <dirent.h>
#include <unistd.h>
#else
#define popen _popen
#define pclose _pclose
#endif
#include "nspr.h"

/*
 * Get a listing of backup directories
 * Return NULL for errors  and a NULL list for an empty list.
 */
 
DS_EXPORT_SYMBOL char **
ds_get_bak_dirs()
{
    char    format_str[PATH_MAX];
    int        i = 0;
    char    **bak_dirs = NULL;
    char    *bakdir = NULL;

    if ( (bakdir = ds_get_bak_dir()) == NULL )
    {
        ds_send_error("Cannot find backup directory.", 0);
        return(bak_dirs);
    }

    PR_snprintf( format_str, PATH_MAX, "%s", bakdir );
    bak_dirs = ds_get_file_list( format_str );
    if( bak_dirs )
    {
        while( bak_dirs[i] != NULL )
        {
            /* Prepend the filename with the backup directory */
            char filename[PATH_MAX];
            PR_snprintf( filename, PATH_MAX, "%s%c%s",
                            bakdir, FILE_SEP, bak_dirs[i] );
            free( bak_dirs[i] );
            bak_dirs[i] = strdup( filename );
#if defined( XP_WIN32 )
            ds_dostounixpath( bak_dirs[i] );
#endif
            i++;
        }
    }

    return(bak_dirs);
}
