/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * END COPYRIGHT BLOCK **/

#define DATABASE_NAME "ns-admin"

#ifdef RESOURCE_STR

#undef LIBRARY_NAME
#include "../admserv/cgi-src40/dbtcgiadmin.h"
#undef LIBRARY_NAME
#include "libadmin/dbtlibadmin.h"

static res_RESOURCE_GLOBAL allxpstr[] = {
  admserv,
  0
};

#endif /* ifdef RESOURCE_STR */
